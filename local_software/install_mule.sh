#! /bin/bash

source ./install_helpers.sh ""

PKG_NAME="mule"
PKG_INSTALLED_FILE="$SWEET_LOCAL_SOFTWARE_DST_DIR/bin/DOES_NOT_EXIST"

config_setup

config_exec cd ../mule
config_exec ./run_install_mule_python.sh

config_success
