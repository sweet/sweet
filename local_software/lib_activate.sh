
function setup_echo_helpers()
{
    #
    # START: Some convenient functions
    #
    # echo_info [message]:
    #	Output in regular colors
    #
    # echo_success [message]:
    #	Output success message in green color
    #
    # echo_warning [message]:
    #	Output warning message in yellow color
    #
    # echo_error [message]:
    #	Output success message in red color
    #
    # echo_*_hline [message]:
    #	Output a horizontal separator line
    #

    echo_prefix()(	echo -n "MULE: "; )

    # Pretty output
    echo_info()( echo_prefix; echo "${@}"; )
    echo_success()( echo_prefix; echo -en "\033[0;32m"; echo "${@}"; echo -en "\033[0m"; )
    echo_warning()( echo_prefix; echo -en "\033[0;33m"; echo "${@}"; echo -en "\033[0m"; )
    echo_error()( echo_prefix; echo -en "\033[0;31m"; echo "${@}"; echo -en "\033[0m"; )

    # hlines
    echo_info_hline()( echo_info "*************************************************************************"; )
    echo_success_hline()( echo_success "*************************************************************************"; )
    echo_warning_hline()( echo_warning "*************************************************************************"; )
    echo_error_hline()( echo_error "*************************************************************************"; )

    # output error and exit
    echo_error_exit(){ echo_error_hline; echo_prefix; echo -en "\033[0;31m"; echo "${@}"; echo -en "\033[0m"; echo_error_hline; exit 1; }
    #echo_error_return(){ echo_error_hline; echo_prefix; echo -en "\033[0;31m"; echo "${@}"; echo -en "\033[0m"; echo_error_hline; return; }

    echo_exec()( echo_prefix; echo "Executing '${@}'"; ${@})

    export -f echo_prefix
    export -f echo_info echo_success echo_warning echo_error
    export -f echo_info_hline echo_success_hline echo_warning_hline echo_error_hline
    export -f echo_error_exit
    export -f echo_exec
}


function test_using_bash()
{
	#
	# Make sure this script to be included as a source and not executed.
	# If it's executed, stop here.
	#

	if [ "$(uname)" == "Darwin" ]; then
		if [ "$0" != "bash" ]; then
			echo_error_hline
			echo_error "These scripts are only compatible to the bash shell"
			echo_error_hline
			return 1
		fi
	else

		if [ "`basename -- "${SHELL}"`" != "bash" ] && [ "$0" != "bash" ]; then
			echo_error_hline
			echo_error "These scripts are only compatible to the bash shell"
			echo_error_hline
			return 1
		fi
	fi

	return 0
}


function test_activate_script_was_included()
{
	#
	# Make sure this script to be included as a source and not executed.
	#

	if [ "$(uname)" != "Darwin" ]; then
		if [ "#$(basename -- $0)" = "#activate.sh" ]; then
			echo_error_hline
			echo_error "THIS SCRIPT MAY NOT BE EXECUTED, BUT INCLUDED IN THE ENVIRONMENT VARIABLES!"
			echo_error_hline
			echo_error "Use e.g. "
			echo_error ""
			echo_error "   $ source ./activate.sh"
			echo_error ""
			echo_error "to setup the environment variables correctly"
			echo_error_hline
			return 1
		fi
	fi
	return 0
}


function mule_load_local_conf()
{
	#
	# Load local configuration, if it exists
	#

	MLP="${MULE_SOFTWARE_ROOT_DIR}/mule_local.conf"
	if [ -f "${MLP}" ]; then
		echo_info "Found '${MLP}' and will import it."
		source "${MLP}"
	fi
}


function activate_miniconda_environment()
{
	#
	# This activates the miniconda environment.
	# This can be linked up with
	# 	PROMPT_COMMAND='activate_miniconda_environment'
	#

	if [[ ! -e "${MULE_PYTHON_VENV_DIR}/bin/activate" ]]; then
		#echo "Miniconda not found, skipping activation"
		return 0
	fi

	source "${MULE_PYTHON_VENV_DIR}/bin/activate" 2>/dev/null

	echo_info "Miniconda activated - disabling prompt command"
	export PROMPT_COMMAND="${PROMPT_COMMAND/activate_miniconda_environment;/}"
	export PROMPT_COMMAND="${PROMPT_COMMAND/activate_miniconda_environment/}"
	return 0
}
export -f activate_miniconda_environment


function setup_miniconda_environment_prompt_command()
{
	#
	# Bash provides an environment variable called PROMPT_COMMAND.
	# The contents of this variable are executed as a regular bash
	# command just before Bash displays a prompt.
	#

	if [[ -z "${PROMPT_COMMAND+x}" ]]; then
		export PROMPT_COMMAND="activate_miniconda_environment"
	else
		export PROMPT_COMMAND="activate_miniconda_environment;${PROMPT_COMMAND}"
	fi

	activate_miniconda_environment
}


function mule_backup_environment()
{
	#
	# Backup particular the environment variables
	#

	export MULE_BACKUP_CC="$CC"
	export MULE_BACKUP_CXX="$CXX"
	export MULE_BACKUP_FC="$FC"
	export MULE_BACKUP_F90="$F90"
	export MULE_BACKUP_FPP="$FPP"

	export MULE_BACKUP_PATH="${PATH}"
	export MULE_BACKUP_PKG_CONFIG_PATH="${PKG_CONFIG_PATH}"
	export MULE_BACKUP_DYLD_LIBRARY_PATH="${DYLD_LIBRARY_PATH}"
	export MULE_BACKUP_LD_LIBRARY_PATH="${LD_LIBRARY_PATH}"
	export MULE_BACKUP_LDFLAGS="${LDFLAGS}"
	export MULE_BACKUP_PYTHONPATH="${PYTHONPATH}"
	export MULE_BACKUP_PS1="${PS1}"
	export MULE_BACKUP_C_INCLUDE_PATH="${C_INCLUDE_PATH}"
	export MULE_BACKUP_F_INCLUDE_PATH="${F_INCLUDE_PATH}"
}


function mule_unload_environment()
{
	#
	# Undo all changes to the environment variables done by this script
	#
	echo_info "Unloading MULE environment"

	# Restore backed up variables
	for var in $(env | grep -o '^MULE_BACKUP_[^=]*'); do
		vvar="${var/#MULE_BACKUP_/}"

		if [[ -z "${!var}" ]]; then
			# Remove the variable if it was empty
			unset ${vvar}
		else
			export ${vvar}="${!var}"
		fi
		unset "${var}"
	done

	# Unset the rest of the MULE_ prefixed variables
	for var in $(env | grep -o '^MULE_[^=]*'); do
		unset "${var}"
	done
}


function test_mule_already_loaded()
{
	#
	# Check if MULE_SOFTWARE_ROOT_DIR environment is already setup
	#

	if [ ! -z ${MULE_SOFTWARE_ROOT_DIR+x} ]; then
		# Use 'echo' here since the 'echo_warning' functions might not be loaded, yet
		echo "Environment variables already loaded for platform '${MULE_PLATFORM_ID}' (skipping)"
		return 1
	fi

	return 0
}


function setup_mule_basic_environment()
{
	#
	# Setup important directory environment variables
	#

	# Get SOFTWARE root directory
	export MULE_SOFTWARE_ROOT_DIR=$(realpath "${MULE_LOCAL_SOFTWARE_DIR}/../")

	# Setup maximum number of jobs to be used for
	# local_software compilation
	export MULE_LOCAL_SOFTWARE_COMPILE_NUM_JOB_LIMITATION=-1
}


function setup_mule_platform()
{
	#
	# Setup platform specific parts
	#

	source ${MULE_SOFTWARE_ROOT_DIR}/platforms/load_platform.sh $@ || return 1
}


function setup_mule_platform_environment()
{
	#
	# Setup environment variables based on platform
	#
	export MULE_LOCAL_SOFTWARE_SRC_DIR="${MULE_LOCAL_SOFTWARE_DIR}/src_cache/"
	export MULE_LOCAL_SOFTWARE_BUILD_DIR="${MULE_LOCAL_SOFTWARE_DIR}/build_${MULE_PLATFORM_ID}/"
	export MULE_LOCAL_SOFTWARE_ENV_DIR="${MULE_LOCAL_SOFTWARE_DIR}/env_${MULE_PLATFORM_ID}/"

	export PATH="${MULE_LOCAL_SOFTWARE_ENV_DIR}/bin:${PATH}"

	if [[ -d "${MULE_SOFTWARE_ROOT_DIR}/bin" ]]; then
		export PATH="${MULE_SOFTWARE_ROOT_DIR}/bin:${PATH}"
	fi

	if [[ ! -z "${PKG_CONFIG_PATH+x}" ]]; then
		export PKG_CONFIG_PATH="${MULE_LOCAL_SOFTWARE_ENV_DIR}/lib/pkgconfig:${PKG_CONFIG_PATH}"
	else
		export PKG_CONFIG_PATH="${MULE_LOCAL_SOFTWARE_ENV_DIR}/lib/pkgconfig"
	fi

	if [[ ! -z "${LD_LIBRARY_PATH+x}" ]]; then
		# Setup LD_LIBRARY_PATH which stored additional library paths
		export LD_LIBRARY_PATH="${MULE_LOCAL_SOFTWARE_ENV_DIR}/lib:${LD_LIBRARY_PATH}"
	else
		export LD_LIBRARY_PATH="${MULE_LOCAL_SOFTWARE_ENV_DIR}/lib"
	fi

	# Always include lib64 even if it doesn exist.
	# Thats important to install software into this directory
	export LD_LIBRARY_PATH="${MULE_LOCAL_SOFTWARE_ENV_DIR}/lib64:${LD_LIBRARY_PATH}"

	# Some MacOS stuff
	export DYLD_LIBRARY_PATH="${MULE_LOCAL_SOFTWARE_ENV_DIR}/lib:${LD_LIBRARY_PATH}"
	if [ -d "${MULE_LOCAL_SOFTWARE_ENV_DIR}/lib64" ]; then
		export DYLD_LIBRARY_PATH="${DYLD_LIBRARY_PATH}:${MULE_LOCAL_SOFTWARE_ENV_DIR}/lib64:${LD_LIBRARY_PATH}"
	fi

	# Update LDFLAGS with lib64 and lib
	if [[ ! -z "${LDFLAGS+x}" ]]; then
		export LDFLAGS="-L${MULE_LOCAL_SOFTWARE_ENV_DIR}/lib64 -L${MULE_LOCAL_SOFTWARE_ENV_DIR}/lib ${LDFLAGS}"
	else
		export LDFLAGS="-L${MULE_LOCAL_SOFTWARE_ENV_DIR}/lib64 -L${MULE_LOCAL_SOFTWARE_ENV_DIR}/lib"
	fi

	# Update include path
	if [[ ! -z "${C_INCLUDE_PATH+x}" ]]; then
		export C_INCLUDE_PATH="${MULE_LOCAL_SOFTWARE_ENV_DIR}/include:${C_INCLUDE_PATH}"
	else
		export C_INCLUDE_PATH="${MULE_LOCAL_SOFTWARE_ENV_DIR}/include"
	fi

	if [[ ! -z "${F_INCLUDE_PATH+x}" ]]; then
		export F_INCLUDE_PATH="${MULE_LOCAL_SOFTWARE_ENV_DIR}/include:${F_INCLUDE_PATH}"
	else
		export F_INCLUDE_PATH="${MULE_LOCAL_SOFTWARE_ENV_DIR}/include"
	fi

	# Python venv directory
	export MULE_PYTHON_VENV_DIR="${MULE_LOCAL_SOFTWARE_ENV_DIR}/python_venv_miniconda/"

}


function setup_shell_prompt_prefix()
{
	# Test if 'realpath' exists
	type realpath >/dev/null 2>&1
	if [[ $? -eq 0 ]]; then

		if [[ "${OSTYPE}" == "darwin"* ]]; then
			# --relative-base option doens't exist on MacOS
			MULE_SHELL_PATH='$(realpath ./)'
		else
			MULE_SHELL_PATH='$(realpath  --relative-base=${MULE_SOFTWARE_ROOT_DIR} ./)'
		fi
		export PS1="\[\033[01;32m\][${MULE_PLATFORM_ID}]\[\033[00m\] ${MULE_SHELL_PATH}\$ "
	else
		MULE_SHELL_PATH='\w'
		export PS1="\[\033[01;32m\][${MULE_PLATFORM_ID}]\[\033[00m\] ${MULE_SHELL_PATH}\$ "
	fi
}
