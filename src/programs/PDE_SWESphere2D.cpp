/*
 * Author: Martin SCHREIBER <schreiberx@gmail.com>
 *
 * MULE_COMPILE_FILES_AND_DIRS: src/programs/PDE_SWESphere2D/
 * MULE_COMPILE_FILES_AND_DIRS: src/programs/PDE_SWESphere2D/TimeOld
 * MULE_COMPILE_FILES_AND_DIRS: src/programs/PDE_SWESphere2D/TimeTree
 * MULE_COMPILE_FILES_AND_DIRS: src/programs/PDE_SWESphere2D/TimeTree/TimeStepper
 * MULE_COMPILE_FILES_AND_DIRS: src/programs/PDE_SWESphere2D/Benchmarks
 *
 * MULE_SCONS_OPTIONS: --sphere2d-spectral-space=enable
 */

#include <sweet/Tools/DefaultPrecompilerValues.hpp>

#if SWEET_MPI
#	include <mpi.h>
#endif

#include "PDE_SWESphere2D/Program.hpp"


#if SWEET_MPI
int mpi_comm_rank;
int mpi_comm_size;
#endif

bool isMPIRoot()
{
#if SWEET_MPI
	return mpi_comm_rank == 0;
#else
	return true;
#endif
}


int main_mpi(int i_argc, char *i_argv[])
{
#if SWEET_MPI
	#if SWEET_THREADING
		int provided;
		MPI_Init_thread(&i_argc, &i_argv, MPI_THREAD_MULTIPLE, &provided);

		if (provided != MPI_THREAD_MULTIPLE)
			SWEETErrorFatal("MPI_THREAD_MULTIPLE not available! Try to get an MPI version with multi-threading support or compile without OMP/TBB support. Good bye...");
	#else
		MPI_Init(&i_argc, &i_argv);
	#endif

	MPI_Comm_rank(MPI_COMM_WORLD, &mpi_comm_rank);
	MPI_Comm_size(MPI_COMM_WORLD, &mpi_comm_size);
#endif

	PDE_SWESphere2D::Program simulation(i_argc, i_argv);
	ERROR_CHECK_WITH_PRINT_AND_COND_RETURN_EXITCODE(simulation);

	if (!simulation.setup())
	{
		SWEET_ASSERT(simulation.error.exists());
		ERROR_CHECK_WITH_PRINT_AND_COND_RETURN_EXITCODE(simulation);
	}

	{
		ERROR_CHECK_WITH_PRINT_AND_COND_RETURN_EXITCODE(simulation);

#if SWEET_GUI
		if (simulation.shackIOData->guiEnabled)
		{
			sweet::GUI::VisSweet visSweet(simulation);
		}
		else
#endif
		{
			simulation.shackTimestepControl->validateMaxSimulationTimeOrTimestepNr();
			ERROR_CHECK_WITH_PRINT_AND_COND_RETURN_EXITCODE(*(simulation.shackTimestepControl));

			if (simulation.shackPDESWESphere2D->normal_mode_analysis_generation > 0)
			{
				simulation.normalmode_analysis();
			}
			else
			{
				simulation.timestepHandleOutput();

				sweet::Tools::StopwatchBox::getInstance().main_timestepping.start();

				while (!simulation.should_quit())
				{
					simulation.runTimestep();

					if (simulation.shackPDESWESphere2D->instability_checks)
					{
						if (isMPIRoot())
						{
							if (simulation.detect_instability())
							{
								std::cerr << "INSTABILITY DETECTED" << std::endl;
								exit(1);
								break;
							}
						}
					}

					simulation.timestepHandleOutput();
				}

				if (isMPIRoot())
					std::cout << "TIMESTEPPING FINISHED" << std::endl;

				sweet::Tools::StopwatchBox::getInstance().main_timestepping.stop();
			}

			if (isMPIRoot())
			{
				if (simulation.fileOutput.output_reference_filenames.size() > 0)
					std::cout << "[MULE] reference_filenames: " << simulation.fileOutput.output_reference_filenames << std::endl;
			}
		}

		// End of run output results
		simulation.output_timings();
	}

	ERROR_CHECK_WITH_PRINT_AND_COND_RETURN_EXITCODE(simulation);

	if (isMPIRoot())
		std::cout << "FIN" << std::endl;

#if SWEET_MPI
	MPI_Finalize();
#endif

	return 0;
}


int main(int i_argc, char *i_argv[])
{
	int retval = main_mpi(i_argc, i_argv);

	//MemBlockAlloc::shutdown();

	return retval;
}

