#ifndef PROGRAMS_PDE_SWESPHERE2D_TIMETREE_TIMESTEPPER_LN_HPP
#define PROGRAMS_PDE_SWESPHERE2D_TIMETREE_TIMESTEPPER_LN_HPP


#include <sweet/Data/GenericContainer/CastHelper.hpp>
#include <sweet/Error/Base.hpp>
#include <sweet/TimeTree/TimeTree_Node_LeafHelper.hpp>

#include "../../DataContainer/Simulation.hpp"
#include "../../DataContainer/Config.hpp"
#include "../../Shack.hpp"
#include "../../Benchmarks/Shack.hpp"
#include "../../BenchmarksCombined.hpp"

/*
 * Time tree node related includes
 */

namespace PDE_SWESphere2D {
namespace TimeTree {
namespace TimeStepper {


class ln	:
	public sweet::TimeTree::TimeTree_Node_LeafHelper<ln>,
	public sweet::Data::GenericContainer::CastHelper<
			DataContainer::Simulation,
			DataContainer::Config
		>
{
private:
	sweet::Shacks::Dictionary *_shackDict;
	Shack *_shackPDESWESphere2D;
	PDE_SWESphere2D::Benchmarks::Shack *_shackPDESWESphere2DBenchmarks;
	const sweet::Data::Sphere2D::Operators *_ops;

	/*
	 * Coriolis effect
	 */
	sweet::Data::Sphere2D::DataGrid _fg;

	Benchmarks::BenchmarksCombined sphere2DBenchmarks;
	DataContainer::Topography topography;


public:
	ln();
	~ln();

	ln(
			const ln &i_val
	);

public:
	bool shackRegistration(
			sweet::Shacks::Dictionary *io_shackDict
	) override;

	const std::vector<std::string> getNodeNames() override;

public:
	bool outputHelp(
			std::ostream &o_ostream,
			const std::string &i_prefix = ""
	) override;

	bool setupConfigAndForwardTimeStepperEval(
		const sweet::Data::GenericContainer::ConfigBase &i_deTermConfig,
		TIME_STEPPER_TYPES i_evalType,
		TimeTree_Node_Base::EvalFun *o_timeStepper
	) override;

	bool _setupTopography();

	void clear() override;

	bool _eval_tendencies(
			const sweet::Data::GenericContainer::Base &i_u,
			sweet::Data::GenericContainer::Base &o_u,
			double i_timeStamp
	)	override;
};

}}}

#endif
