#! /usr/bin/env python3


url_repos = "https://gitlab.inria.fr/sweet/sweet.git"

#
# Where to write the gitlab_ci_file to
#
gitlab_ci_file="gitlab_ci.yml"

#
# SWEET's root directory
#
# Relative to this path, the tests are searched for
#
sweet_root_directory="../"


"""
Also create test scripts to try out tests directly with docker images?
"""
#create_test_scripts_with_docker = False
create_test_scripts_with_docker = True


####################################################
# Setup finishes here
####################################################

import sys
import os
import glob
import platform
import re
from itertools import product

# Import all test environment descriptions
from test_environments import *


verbosity = 10
if len(sys.argv) > 1:
    verbosity = int(sys.argv[1])

if verbosity >= 10:
    print("Working directory: "+os.path.abspath(os.curdir))
    print("Setting up tests in Gitlab CI file '"+gitlab_ci_file+"'")


def get_test_id(test):
    r = re.match(r".*/([0-9_a-zA-Z]*)/test\.[sp][hy]", test)
    if r == None:
        raise Exception("Invalid test file found")

    return r.group(1)

def get_test_id_with_script(test):
    r = re.match(r".*/([0-9_a-zA-Z]*/test\.[sp][hy])", test)
    if r == None:
        raise Exception(f"Invalid test file '{test}' found")

    return r.group(1)

#
# Load all tests
#
tests_raw = glob.glob(f"{sweet_root_directory}/tests/??_*/test.sh")
tests_raw += glob.glob(f"{sweet_root_directory}/tests/??_*/test.py")

# Sort tests
tests_raw.sort()

# For debugging
if False:
    _tests_raw = []
    for t in tests_raw:
        # Filter out only the tests with 'DEBUG' in their name
        if 'DEBUG' in t:
            _tests_raw.append(t)

    tests_raw = _tests_raw


"""
Create dictionary with
    'test id' => 'relative path to script'
"""
tests_dict = {}
for test_raw in tests_raw:
    tests_dict[get_test_id(test_raw)] = get_test_id_with_script(test_raw)


"""
Create special dictionary with less tests
"""

tests_compile_list = """\
10_compile
""".split("\n")


tests_dict_compile = {}
for test_id in tests_dict:
    if test_id in tests_compile_list:
        tests_dict_small[test_id] = tests_dict[test_id]



job_counter = 0

if verbosity >= 10:
    for test in tests_dict:
        print(f" + Found test '{test}' with script '{tests_dict[test]}'")

gitlab_ci_content = ""

gitlab_ci_content += """\
#
# Gitlab CI file for SWEET
#
# Warning! This file is automatically generated. DO NOT MODIFY!
#

include: 
  - gitlab_ci/templates.yml

stages:          # List of stages for jobs, and their order of execution
  - stage-docker-build
"""

for te in test_environment_:
    if te['gen_ci_tests'] != None:
        gitlab_ci_content += f"""\
  - stage_tests_{te['id']}
"""

gitlab_ci_content += f"""\
  - stage-doc-build
"""

gitlab_ci_content += f"""\

"""



"""
Also create test scripts to try out tests directly with docker images?
"""
test_scripts_dir = './sweet_tests_docker_scripts'

if create_test_scripts_with_docker:
    try:
        os.makedirs(test_scripts_dir)
    except:
        pass


#
# Setup Docker Images with local software for each environment
#
# These Docker Images are only build if
#
# * the commit message includes
#   [docker-build]
#   including the [ ] brackets
#   or
#
# * there's a tag
#
# This is part of the file template.yml
#

def create_gitlab_ci_content_docker_build(te):
    return f"""\

setup-docker-images-{te['id']}:       # This job runs in the build stage, which runs first.
{te['job_tags_docker_build']}\
  stage: stage-docker-build
  variables:
    IMAGE_NAME: {te['docker_image_id']}
  extends:
    .docker-build
"""

def create_gitlab_ci_content_test(te, test_id, test_script, prerun_commands, create_test_scripts_with_docker):

    job_id = test_id

    run_commands = []
    run_commands += [f"./tests/{test_script}"]
#    run_commands += [f"echo \"TEST {job_id} SUCCESSFULLY FINISHED\""]

    test_job_id = f"test-{te['id']}-{job_id}"

    ci_run_commands = ""
    for c in run_commands:
        ci_run_commands += "    - "+c+"\n"

    gitlab_ci_content = ""
    gitlab_ci_content += f"""\

# Template for this job
.{test_job_id}:
  stage: stage_tests_{te['id']}
  image: $CI_REGISTRY_IMAGE/sources/{te['docker_image_id']}
{te['job_tags']}\
  script:
{ci_run_commands}


# With Docker image DAG dependency
{test_job_id}:
  # Job dependency
  needs: ["setup-docker-images-{te['id']}"]
  extends:
    - .{test_job_id}
    - .prerun_{te['id']}
    - .rules-tests-norun-and-docker-rules


# WITHOUT Docker Image DAG dependency 
{test_job_id}_:
  # Job dependency
  needs: []
  extends:
    - .{test_job_id}
    - .prerun_{te['id']}
    - .rules-tests-norun-and-docker-rules-inv
"""

    if create_test_scripts_with_docker:

        script_path = test_scripts_dir+"/"+test_job_id+".sh"

        script_commands = ""

        c_ = prerun_commands+run_commands

        # Ensure that the scripts also use the gitlab_ci platform
        c_ = ["export GITLAB_CI=1"] + c_

        # This is done by gitlab CI and we need to do it manually right here
        script_commands += f"mkdir dummy && cd dummy && git clone --branch $(git rev-parse --abbrev-ref HEAD) --depth 1 \"{url_repos}\" sweet_src; cd sweet_src; "

        for c in c_:

            # Echo what we're executing
            escaped = c[:]

            if "[" in escaped or "$" in escaped:
                escaped = escaped.replace("[", "\\[")
                escaped = escaped.replace("]", "\\]")
                # TODO: Also create echo for this one
            else:
                script_commands += f"echo \"RUN {escaped}\" && "

            escaped = escaped.replace("$", "\\$")

            # Execute command
            script_commands += f"{escaped}"

            # Dead with exports which require a ; at the end
            if c.startswith("export "):
                script_commands += "; "
            else:
                script_commands += " && "

        script_commands += "echo \"FINISHED\""

        escaped_script_commands = script_commands.replace("\"", "\\\"")

        container_name = f"test_run_"+te['docker_image_id']

        script_content = f"""\
#! /bin/bash
"""

        # First some cleanup in case that the container already exists
        script_content += f"""
function kill_test_container() {{
    echo "Killing existing containers..."
    if [[ "$1" == "no_output" ]]; then
        docker stop {container_name} 2>&1 > /dev/null
        docker rm {container_name} 2>&1 > /dev/null
    else
        docker stop {container_name}
        docker rm {container_name}
    fi
}}


kill_test_container no_output

trap "kill_test_container" INT

if [ ! -z "$1" ]; then 
    export GIT_BRANCH=$1
fi

if [ ! -z "$2" ]; then 
    export GIT_COMMIT=$2
fi

"""

        script_content += f"""\
docker run --name={container_name} -i -t -d {te['docker_image_id_slash']}

"""
        script_content += f"docker exec {container_name} bash -c \"{escaped_script_commands}\" || exit 1\n"

        script_content += f"kill_test_container\n"
        script_content += f"echo \"FIN\" || exit 1\n"

        with open(script_path, "w") as f:
            f.write(script_content)

        os.chmod(script_path, 0o755)

    return gitlab_ci_content



for te in test_environment_:

    gitlab_ci_content += create_gitlab_ci_content_docker_build(te)

    job_counter += 1

#
# Generate individual test cases
#

#if False:
for te in test_environment_:

    if te['gen_ci_tests'] == None:
        continue

    if te['gen_ci_tests'] == "all":
        create_tests_dict = tests_dict
    elif te['gen_ci_tests'] == "compile":
        create_tests_dict = tests_dict_compile
    else:
        raise Exception("Unknown test cases chosen")

    prerun_commands = []

    # Even if we already exported these variables via Docker,
    # we export them again due some possible overrides
    for env_name in te:
        if 'env_' == env_name[:4]:
            prerun_commands += [f"export {env_name[4:]}={te[env_name]}"]

    # We just show all environment variables
    prerun_commands += [f"export GIT_CLONE_DIR=\"$(pwd)\""]
    prerun_commands += [f"export"]

    if platform.node() == "time-x":
        prerun_commands += [f"export http_proxy=http://proxy.in.tum.de:8080/"]
        prerun_commands += [f"export https_proxy=http://proxy.in.tum.de:8080/"]

    prerun_commands += [f"pwd"]
    prerun_commands += [f"ls -lh"]
    prerun_commands += [f"cd \"{sweet_docker_dir}\""]
    prerun_commands += [f"mv \"$GIT_CLONE_DIR\" sweet_src"]
    prerun_commands += [f"cd sweet_src"]

    #prerun_commands += [f"if [[ \"$GIT_COMMIT\" != \"\" ]]; then echo git checkout \"$GIT_COMMIT\"; git checkout \"$GIT_COMMIT\"; else echo \"No commit checkout\"; fi"]
    prerun_commands += [f"git branch -vv"]
    prerun_commands += [f"mv ../local_backup ./local_software/local"]
    prerun_commands += [f"source ./activate.sh"]

    ci_prerun_commands = ""
    for c in prerun_commands:
        ci_prerun_commands += "    - "+c+"\n"

    gitlab_ci_content += f"""\

.prerun_{te['id']}:
  before_script:
{ci_prerun_commands}

"""

    for test in create_tests_dict:
        gitlab_ci_content += create_gitlab_ci_content_test(te, test, create_tests_dict[test], prerun_commands, create_test_scripts_with_docker)

        job_counter += 1

"""
We then add a job for automatically triggering the creation of documentation for the repository. This is done by triggering the pipeline of sweet-doc.
This job is only run when there is a push/merge into main branch AND when all other tests have successfully finished".
"""
gitlab_ci_content+="""

trigger-sweet-doc:
  image: registry.gitlab.inria.fr/inria-ci/docker/ubuntu:22.04
  stage: stage-doc-build
  when: on_success
  tags:
    - ci.inria.fr
    - small
  script:
    - apt-get update && apt-get install -y curl git jq 
    - curl -X POST --fail -F token=glptt-6cb9be3b06c9cc6151a73ab32edc37df8eb845b4 -F ref=main https://gitlab.inria.fr/api/v4/projects/46061/trigger/pipeline      
  rules:
    - if: $CI_PIPELINE_SOURCE == "push" && $CI_COMMIT_REF_NAME == "main" 
    """
"""
Next, we compress the .yml a little bit
"""
if True:
    import re

    """
    Remove lines which are only comments
    """
    pattern = r"^([ \t]*#[^\r\n]*[\r\n])"
    regex = re.compile(pattern, re.MULTILINE|re.DOTALL)

    def callback(match):
        return "" if match.group(1) is not None else match.group(0)

    gitlab_ci_content = regex.sub(callback, gitlab_ci_content)

if True:
    import re

    """
    Remove double white lines
    """
    gitlab_ci_content = gitlab_ci_content.replace("\n\n", "\n")


if True:
    import re

    """
    Remove all comments to save space
    """
    pattern = r"(\".*?\"|\'.*?\')|([ \t]*#[^\r\n]*$)"
    regex = re.compile(pattern, re.MULTILINE|re.DOTALL)

    def callback(match):
        return "" if match.group(2) is not None else match.group(1)

    gitlab_ci_content = regex.sub(callback, gitlab_ci_content)

print(gitlab_ci_content)


if verbosity >= 10:
    print("Writing content to file '"+gitlab_ci_file+"'")

with open(gitlab_ci_file, 'w') as f:
    f.write(gitlab_ci_content)


if verbosity >= 10:
    print(f"Job counter: {job_counter}")

