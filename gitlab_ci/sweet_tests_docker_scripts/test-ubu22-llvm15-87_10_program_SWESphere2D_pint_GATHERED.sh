#! /bin/bash

function kill_test_container() {
    echo "Killing existing containers..."
    if [[ "$1" == "no_output" ]]; then
        docker stop test_run_sweet-ubu22-llvm15 2>&1 > /dev/null
        docker rm test_run_sweet-ubu22-llvm15 2>&1 > /dev/null
    else
        docker stop test_run_sweet-ubu22-llvm15
        docker rm test_run_sweet-ubu22-llvm15
    fi
}


kill_test_container no_output

trap "kill_test_container" INT

if [ ! -z "$1" ]; then 
    export GIT_BRANCH=$1
fi

if [ ! -z "$2" ]; then 
    export GIT_COMMIT=$2
fi

docker run --name=test_run_sweet-ubu22-llvm15 -i -t -d sweet/ubu22-llvm15

docker exec test_run_sweet-ubu22-llvm15 bash -c "mkdir dummy && cd dummy && git clone --branch $(git rev-parse --abbrev-ref HEAD) --depth 1 \"https://gitlab.inria.fr/sweet/sweet.git\" sweet_src; cd sweet_src; echo \"RUN export GITLAB_CI=1\" && export GITLAB_CI=1; echo \"RUN export CC=clang-15\" && export CC=clang-15; echo \"RUN export CXX=clang++-15\" && export CXX=clang++-15; echo \"RUN export LINK=clang++-15\" && export LINK=clang++-15; echo \"RUN export LD=ld\" && export LD=ld; echo \"RUN export F90=gfortran-12\" && export F90=gfortran-12; echo \"RUN export FC=gfortran-12\" && export FC=gfortran-12; export GIT_CLONE_DIR=\"\$(pwd)\"; echo \"RUN export\" && export && echo \"RUN pwd\" && pwd && echo \"RUN ls -lh\" && ls -lh && echo \"RUN cd \"/sweet_docker\"\" && cd \"/sweet_docker\" && mv \"\$GIT_CLONE_DIR\" sweet_src && echo \"RUN cd sweet_src\" && cd sweet_src && echo \"RUN git branch -vv\" && git branch -vv && echo \"RUN mv ../local_backup ./local_software/local\" && mv ../local_backup ./local_software/local && echo \"RUN source ./activate.sh\" && source ./activate.sh && echo \"RUN ./tests/87_10_program_SWESphere2D_pint_GATHERED/test.sh\" && ./tests/87_10_program_SWESphere2D_pint_GATHERED/test.sh && echo \"FINISHED\"" || exit 1
kill_test_container
echo "FIN" || exit 1
