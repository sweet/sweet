#! /bin/bash


for i in sweet_tests_docker_scripts/*.sh; do
	echo "Running '$i'"
	./$i $@ || exit 1
done
