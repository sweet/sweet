#! /usr/bin/env python3

import sys
import os

from test_environments import *



#
# Docker image builders
#
for te in test_environment_:
    print(f"Setting up '{te['id']}'")

    if True:
        dir_path = f"dockerfiles/{te['docker_image_id']}/"
        print(f" + Dockerfile: '{dir_path}'")

        try:
            os.makedirs(dir_path)
        except:
            pass

        content = f"""\
FROM {te['docker_image_base_version']}
SHELL ["/bin/bash", "-c"]
ENV TZ=Europe/Paris

"""

        for env_name in te:
            if 'env_' == env_name[:4]:
                content += f"ENV {env_name[4:]}={te[env_name]}\n"

        content += f"""\
{te['runs_install_packages']}
"""

        content += f"""

RUN echo "Creating SWEET directory to install software to"  \\
    && mkdir -p "{sweet_docker_dir}"   \\
    && echo "FIN"

# Create non-root user 'sweet'
RUN useradd sweet -s /bin/bash
RUN chown sweet "{sweet_docker_dir}"
WORKDIR "{sweet_docker_dir}"

# Switch to non-root user 'sweet'
USER sweet

RUN echo "Cloning SWEET git repository" \\
    && git clone -b docker-build --depth 1 https://gitlab.inria.fr/sweet/sweet.git "{sweet_docker_dir}/sweet_src" \\
    && echo "FIN"

{te['runs_install_local_software']}

RUN echo "Creating backup of installed SWEET local software..." \\
    && mv "sweet_src/local_software/local" "./local_backup" \\
    && echo "DONE"

# Cleaning up things a little bit
RUN echo "Cleaning up..." \\
    && rm -rf sweet_src \\
    && echo "DONE"

# We're ready to use SWEET
"""

        with open(f"{dir_path}/Dockerfile", "w") as f:
            f.write(content)

    if True:
        docker_build_path = f"dockerfiles/docker_build_{te['docker_image_id']}.sh"
        print(f" + Docker build script: './{docker_build_path}'")

        content = f"""\
#! /bin/bash

SCRIPTDIR=$( cd -- "$( dirname -- "${{BASH_SOURCE[0]}}" )" &> /dev/null && pwd )

if [ "$(hostname)" == "time-x" ]; then
    # Use proxy on time-x
    #PROXY="--build-arg https_proxy=http://proxy.in.tum.de:8080/ --build-arg http_proxy=http://proxy.in.tum.de:8080/"
    echo "X"
fi

docker build -t sweet/{te['id']} ${{PROXY}} "${{SCRIPTDIR}}/{te['docker_image_id']}" $@
"""
        with open(docker_build_path, "w") as f:
            f.write(content)

        os.chmod(docker_build_path, 0o755)

