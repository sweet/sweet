#! /bin/bash


for i in dockerfiles/*.sh; do
	echo "Building with docker build script '$i'"
	./$i $@ || { echo "Error in '$i'"; exit 1; }
done
