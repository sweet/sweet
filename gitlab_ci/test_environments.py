
from config import *
import copy
import sys


# Which directory to use as a common directory setup things
sweet_docker_dir = "/sweet_docker"


"""
List of dictionaries describing the test environments
"""
test_environment_ = []


class TestEnvironment:

    def __init__(self, te = {}):
        self.te = te

    def copy(self):
        return TestEnvironment(copy.deepcopy(self.te))

    def setup_gitlab_ci_kind(self, gitlab_ci_kind):
        self.te["gitlab_ci_kind"] = gitlab_ci_kind

    def setup_os(self, os_sys, os_version):
        """
        Step 1) Choose image and default packages to install

        Multiplexer to different OS variants
        """
        self.te["os"] = os_sys

        if self.te["os"] == "ubuntu":
            self.setup_os_ubuntu(os_version)
        elif self.te["os"] == "debian":
            self.setup_os_debian(os_version)
        elif self.te["os"] == "opensuse":
            self.setup_os_opensuse(os_version)
        elif self.te["os"] == "fedora":
            self.setup_os_fedora(os_version)
        elif self.te["os"] == "mac":
            self.setup_os_mac(os_version)
        else:
            raise Exception("TODO")


    def setup_os_ubuntu(self, os_version:int):
        """
        Step 1) Choose image and default packages to install
        """

        """
        Setup for Ubuntu OS
        """
        self.te["os_version"] = os_version

        self.te["apt_install_packages"] = []
        self.te["runs_install_packages"] = ""

        # Standard apt-get packages
        self.te["apt_install_packages"] += ["git", "make", "automake", "cmake", "python3", "wget", "libssl-dev"]

        if os_version == 18:
            self.te["docker_image_base_version"] = "docker.io/library/ubuntu:18.04"
            if self.te["gitlab_ci_kind"] == "gricad":
                self.te["docker_image_base_version"] = "ubuntu:bionic"

            self.te["apt_install_packages"] += ["pkg-config", "libfreetype6-dev", "libglu1-mesa-dev"]

        elif os_version == 20:
            self.te["docker_image_base_version"] = "docker.io/library/ubuntu:20.04"
            if self.te["gitlab_ci_kind"] == "gricad":
                self.te["docker_image_base_version"] = "ubuntu:focal"

            self.te["apt_install_packages"] += ["pkg-config", "libfreetype-dev", "libopengl-dev", "libglu1-mesa-dev", "libxext-dev"]

        elif os_version == 22:
            self.te["docker_image_base_version"] = "docker.io/library/ubuntu:22.04"
            if self.te["gitlab_ci_kind"] == "gricad":
                self.te["docker_image_base_version"] = "ubuntu:jammy"

            self.te["apt_install_packages"] += ["pkg-config", "libfreetype-dev", "libopengl-dev", "libglu1-mesa-dev", "libxext-dev"]

        else:
            raise Exception("Unsupported version")


    def setup_os_debian(self, os_version:int):
        """
        Step 1) Choose image and default packages to install
        """

        """
        Setup for Ubuntu OS
        """
        self.te["os_version"] = os_version

        self.te["apt_install_packages"] = []
        self.te["runs_install_packages"] = ""

        # Standard apt-get packages
        self.te["apt_install_packages"] += ["git", "gzip", "bzip2", "xz-utils", "make", "automake", "cmake", "python3", "wget", "libssl-dev"]

        if os_version == 9:
            self.te["docker_image_base_version"] = "docker.io/library/debian:stretch"
            self.te["apt_install_packages"] += ["pkg-config", "libfreetype6-dev", "libglu1-mesa-dev"]

            if self.te["gitlab_ci_kind"] == "inria":
                # Disable LD_PRELOAD on Inria CI since this would use an http cache with GIT which is not compatible
                self.te["env_LD_PRELOAD"] = ""


        elif os_version == 10:
            self.te["docker_image_base_version"] = "docker.io/library/debian:buster"
            self.te["apt_install_packages"] += ["pkg-config", "libfreetype6-dev", "libglu1-mesa-dev", "libxext-dev"]

        elif os_version == 11:
            self.te["docker_image_base_version"] = "docker.io/library/debian:bullseye"
            self.te["apt_install_packages"] += ["pkg-config", "libfreetype6-dev", "libopengl-dev", "libglu1-mesa-dev", "libxext-dev"]

        elif os_version == 12:
            self.te["docker_image_base_version"] = "docker.io/library/debian:bookworm"
            self.te["apt_install_packages"] += ["pkg-config", "libfreetype6-dev", "libopengl-dev", "libglu1-mesa-dev", "libxext-dev"]

        else:
            raise Exception("Unsupported version")


    def setup_os_opensuse(self, os_version:int):
        """
        Setup for OpenSUSE
        """
        self.te["os_version"] = os_version

        self.te["zypper_install_packages"] = []
        self.te["runs_install_packages"] = ""

        # Standard zypper packages
        self.te["zypper_install_packages"] += ["tar", "gzip", "bzip2", "xz", "git", "make", "automake", "cmake", "python3", "wget"]

        if os_version == 15.3:
            self.te["docker_image_base_version"] = "opensuse/leap:15.3"
            self.te["zypper_install_packages"] += ["pkg-config", "freetype2-devel", "Mesa-libGL-devel", "libXext-devel", "libopenssl-devel"]

        elif os_version == 15.4:
            self.te["docker_image_base_version"] = "opensuse/leap:15.4"
            self.te["zypper_install_packages"] += ["pkg-config", "freetype2-devel", "Mesa-libGL-devel", "libXext-devel", "libopenssl-3-devel"]

        elif os_version == 15.5:
            self.te["docker_image_base_version"] = "opensuse/leap:15.5"
            self.te["zypper_install_packages"] += ["pkg-config", "freetype2-devel", "Mesa-libGL-devel", "libXext-devel", "libopenssl-3-devel"]

        else:
            raise Exception("Unsupported version")



    def setup_os_fedora(self, os_version:int):
        """
        Setup for Fedora (Open RedHat)
        """
        self.te["os_version"] = os_version

        self.te["dnf_install_packages"] = []
        self.te["runs_install_packages"] = ""

        # Standard dnf packages
        self.te["dnf_install_packages"] += ["tar", "gzip", "bzip2", "xz", "git", "make", "automake", "cmake", "python3", "wget", "openssl-devel"]

        if os_version == 35:
            self.te["docker_image_base_version"] = "docker.io/library/fedora:35"
            self.te["dnf_install_packages"] += ["pkg-config", "freetype-devel", "mesa-libGL-devel", "libXext-devel"]

        elif os_version == 36:
            self.te["docker_image_base_version"] = "docker.io/library/fedora:36"
            self.te["dnf_install_packages"] += ["pkg-config", "freetype-devel", "mesa-libGL-devel", "libXext-devel"]

        elif os_version == 37:
            self.te["docker_image_base_version"] = "docker.io/library/fedora:37"
            self.te["dnf_install_packages"] += ["pkg-config", "freetype-devel", "mesa-libGL-devel", "libXext-devel"]

        elif os_version == 38:
            self.te["docker_image_base_version"] = "docker.io/library/fedora:38"
            self.te["dnf_install_packages"] += ["pkg-config", "freetype-devel", "mesa-libGL-devel", "libXext-devel"]

        elif os_version == 39:
            self.te["docker_image_base_version"] = "docker.io/library/fedora:39"
            self.te["dnf_install_packages"] += ["pkg-config", "freetype-devel", "mesa-libGL-devel", "libXext-devel"]

        else:
            raise Exception("Unsupported version")




    def setup_local_software_direct(self, local_software_install_script, env_vars="", taskset="", extrun=[]):
        ext = ""
        for _ in extrun:
            ext += "    && "+_+" \\\n"


        return  f"""
RUN echo "Installing local software with "{local_software_install_script}"" \\
    && cd "{sweet_docker_dir}/sweet_src" \\
    && . ./activate.sh gitlab_ci \\
    && cd local_software \\
    && time {env_vars} {taskset} ./{local_software_install_script} \\
    && rm -rf ./local_src \\
"""+ext+"""    && echo "DONE"

    """



    def setup_local_software(self):
        """
        Step 2) Setup default local software
        """

        self.te["default_install_scripts"] = []

        self.te["default_install_scripts"] += [
                   "install_miniconda.sh",
                   "install_fftw3.sh",
                   "install_mule.sh",
                   "install_shtns.sh",
                   "install_shtns_python.sh",
                   "install_sdl2.sh",
                   "install_scons.sh",
                   "install_mpich.sh", 
                   "install_eigen3.sh",
                   "install_lapack.sh",
                   "install_numactl.sh",
                   "install_libpfasst_debug.sh",
                   "install_xbraid.sh",
               ]

        self.te["runs_install_local_software"] = ""



    def setup_compiler(self, compiler, compiler_version):
        """
        Step 3) Setup the compiler
        """

        self.te["compiler"] = compiler
        self.te["compiler_version"] = compiler_version

        if platform.node() == "time-x":
            self.te["env_http_proxy"] = "http://proxy.in.tum.de:8080/"
            self.te["env_https_proxy"] = "http://proxy.in.tum.de:8080/"


        if self.te["os"] == "ubuntu":

            self.te["apt_install_packages"] += ["gdb"]
            if self.te["compiler"] == "gcc":

                # Add GCC compiler packages
                self.te["apt_install_packages"] += [f"g++-{compiler_version}", f"gcc-{compiler_version}", f"gfortran-{compiler_version}"]

                self.te["env_CC"] = f"gcc-{compiler_version}"
                self.te["env_CXX"] = f"g++-{compiler_version}"
                self.te["env_F90"] = f"gfortran-{compiler_version}"
                self.te["env_FC"] = f"gfortran-{compiler_version}"
                self.te["env_LINK"] = f"g++-{compiler_version}"
                self.te["env_LD"] = f"ld"

            elif self.te["compiler"] == "llvm":
                self.te["env_CC"] = f"clang-{compiler_version}"
                self.te["env_CXX"] = f"clang++-{compiler_version}"

                self.te["env_LINK"] = f"clang++-{compiler_version}"
                self.te["env_LD"] = f"ld"

                taskset = ""
                # Limit number of tasks since we would run out of memory for compiling LLVM
                if self.te["gitlab_ci_kind"] == "gricad":
                    taskset = "taskset -c 0,1,2,3"
                elif self.te["gitlab_ci_kind"] == "inria":
                    taskset = "taskset -c 0,1,2,3,4,5"

                if compiler_version == 15:
                    #
                    # We need to install cmake before llvm since llvm requires a newer cmake version
                    #
                    # Here, we also need to override the CC and CXX variables
                    #
                    # Version 15 installs "flang-new" which we symlink in the end to flang-15
                    #
                    # We need to add -O2 to make C and Fortran compiled files compatible to each other
                    #

                    self.te["llvm_compile_gcc_version"] = None

                    if self.te["os"] == "ubuntu":
                        if os_version == 18:
                            self.te["llvm_compile_gcc_version"] = 8
                        elif os_version == 20:
                            self.te["llvm_compile_gcc_version"] = 10
                            compiler_version_ = [8,9,10]
                        elif os_version == 22:
                            self.te["llvm_compile_gcc_version"] = 12

                    else:
                        raise Exception("TODO")

                    # Use GNU default Fortran compiler (since MPICH doesn"t support flang, yet)
                    self.te["env_F90"] = f"gfortran-{self.te['llvm_compile_gcc_version']}"
                    self.te["env_FC"] = f"gfortran-{self.te['llvm_compile_gcc_version']}"


                    if self.te["llvm_compile_gcc_version"] == None:
                        # Fallback solution
                        self.te["apt_install_packages"] += ["g++", "gcc", "gfortran"]
                        comp_env = f"CC=gcc CXX=g++"
                    else:
                        self.te["apt_install_packages"] += [f"g++-{self.te['llvm_compile_gcc_version']}", f"gcc-{self.te['llvm_compile_gcc_version']}", f"gfortran-{self.te['llvm_compile_gcc_version']}"]
                        comp_env = f"CC=gcc-{self.te['llvm_compile_gcc_version']} CXX=g++-{self.te['llvm_compile_gcc_version']}"


                    # CMAKE is required in a newer version for  LLVM
                    self.te["runs_install_local_software"] += self.setup_local_software_direct("install_cmake.sh", comp_env)

                    # Install MiniConda before LLVM since some test scripts rely on a newer Python version
                    self.te["runs_install_local_software"] += self.setup_local_software_direct("install_miniconda.sh")
                    self.te["default_install_scripts"].remove("install_miniconda.sh")

                    # Now, it"s time to install LLVM
                    extrun = ["ls -l local/bin/"]
                    self.te["runs_install_local_software"] += self.setup_local_software_direct("install_llvm_15.0.6.sh", comp_env, taskset, extrun)

                else:
                    raise Exception("TODO")


            else:
                raise Exception("TODO")


        elif self.te["os"] == "debian":

            if self.te["compiler"] == "gcc":

                # Add GCC compiler packages
                self.te["apt_install_packages"] += [f"g++-{compiler_version}", f"gcc-{compiler_version}", f"gfortran-{compiler_version}"]

                self.te["env_CC"] = f"gcc-{compiler_version}"
                self.te["env_CXX"] = f"g++-{compiler_version}"
                self.te["env_F90"] = f"gfortran-{compiler_version}"
                self.te["env_FC"] = f"gfortran-{compiler_version}"
                self.te["env_LINK"] = f"g++-{compiler_version}"
                self.te["env_LD"] = f"ld"

            else:
                raise Exception("TODO")

        elif self.te["os"] == "opensuse":

            if self.te["compiler"] == "gcc":

                # Add GCC compiler packages
                self.te["zypper_install_packages"] += [f"gcc{compiler_version}", f"gcc{compiler_version}-c++", f"gcc{compiler_version}-fortran"]

                self.te["env_CC"] = f"gcc-{compiler_version}"
                self.te["env_CXX"] = f"g++-{compiler_version}"
                self.te["env_F90"] = f"gfortran-{compiler_version}"
                self.te["env_FC"] = f"gfortran-{compiler_version}"
                self.te["env_LD"] = f"ld"

            else:
                raise Exception("TODO")

        elif self.te["os"] == "fedora":

            if self.te["compiler"] == "gcc":

                # Add GCC compiler packages
                self.te["dnf_install_packages"] += [f"gcc-{compiler_version}.*", f"gcc-c++-{compiler_version}.*", f"gcc-gfortran-{compiler_version}.*"]

                self.te["env_CC"] = f"gcc"
                self.te["env_CXX"] = f"g++"
                self.te["env_F90"] = f"gfortran"
                self.te["env_FC"] = f"gfortran"
                self.te["env_LINK"] = f"g++"
                self.te["env_LD"] = f"ld"
            else:
                raise Exception("TODO")

        elif self.te["os"] == "mac":

            if self.te["compiler"] == "gcc":

                # Add GCC compiler packages
                self.te["apt_install_packages"] += [f"g++-{compiler_version}", f"gcc-{compiler_version}", f"gfortran-{compiler_version}"]

                self.te["env_CC"] = f"gcc-{compiler_version}"
                self.te["env_CXX"] = f"g++-{compiler_version}"
                self.te["env_F90"] = f"gfortran-{compiler_version}"
                self.te["env_FC"] = f"gfortran-{compiler_version}"
                self.te["env_LINK"] = f"g++-{compiler_version}"
                self.te["env_LD"] = f"ld"
            else:
                raise Exception("TODO")
        else:
            raise Exception("TODO")




    def setup_job_tags(self):
        """
        Setup the job tags
        """

        # Special job tags for INRIA CI
        self.te["job_tags"] = ""
        self.te["job_tags_docker_build"] = ""

        if self.te["gitlab_ci_kind"] == "inria":
            self.te["job_tags"] = """\
  tags:
    - ci.inria.fr
    - linux
    - large
"""

        if self.te["gitlab_ci_kind"] == "inria" and self.te["compiler"] == "llvm":
            self.te["job_tags_docker_build"] = """\
  tags:
    - ci.inria.fr
    - linux
    - very-large
"""
        else:
            self.te["job_tags_docker_build"] = self.te["job_tags"][:]


    def setup_finalize(self):
        self.te["runs_install_packages"] += f"""\
RUN echo "Generic system information"\\
    && echo ""                  \\
    && echo "ls -lh"            \\
    && ls -lh                   \\
    && echo ""                  \\
    && echo "/proc/cpuinfo"     \\
    && cat /proc/cpuinfo        \\
    && echo ""                  \\
    && echo "uname -a"          \\
    && uname -a                 \\
    && echo ""                  \\
    && echo "cat /etc/os-release"  \\
    && cat /etc/os-release      \\
    && echo "DONE"

"""
        """
        Step 1 postprocessing) Which packages to install
        """
        if self.te["os"] in ["ubuntu", "debian"]:
            if self.te["gitlab_ci_kind"] == "inria":
                if self.te["os"] == "debian" and self.te["os_version"] == 9:

                    #
                    # Workaround for error message
                    # "gnutls_handshake() failed: Decode error"
                    # once we like to run the tests
                    #
                    # https://ci.inria.fr/doc/page/gitlab/
                    #
                    # We also added this environment variable override for the test runs
                    #
                    self.te["runs_install_packages"] += f"""\
RUN echo "Disabling cache preloading library on INRIA"
ENV LD_PRELOAD=

"""
            self.te["runs_install_packages"] += f"""\
RUN echo "Installing packages"  \\
    && ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone \\
    && apt-get -y update \\
    && apt-get -y upgrade \\
    && apt-get install -y -q {" ".join(self.te["apt_install_packages"])} \\
    && apt-get autoclean -y \\
    && apt-get autoremove -y \\
    && rm -rf /var/lib/apt/lists/*  \\
    && echo "DONE"
"""

        elif self.te["os"] == "opensuse":
            self.te["runs_install_packages"] += f"""\
RUN echo "Installing packages"\\
    && zypper -n refresh \\
    && zypper -n update \\
    && zypper -n install {" ".join(self.te["zypper_install_packages"])} \\
    && zypper -n clean \\
    && echo "DONE"
"""

        elif self.te["os"] == "fedora":
            self.te["runs_install_packages"] += f"""\
RUN echo "Installing packages"\\
    && dnf upgrade -y \\
    && dnf install -y {" ".join(self.te["dnf_install_packages"])} \\
    && dnf clean all    \\
    && echo "DONE"
"""

        elif self.te["os"] == "mac":
            self.te["runs_install_packages"] += f"""\
RUN echo "Installing packages"\\
    && echo "TODO" \\
    && exit 1 \\
    && echo "DONE"
"""
        else:
            raise Exception("TODO")

        """
        Step 3) Postprocessing: Which local software to install
        """
        for _ in self.te["default_install_scripts"]:

            env_vars = ""
            if False:
                # Handled in install_shtns_* script

                if self.te["compiler"] == "gcc":
                    if self.te["env_CC"] == "gcc-6":
                        if "_shtns" in _:
                            # Special workaround for shtns with gcc-6 toolchain to get it compiled
                            env_vars = "CFLAGS=-mtune=generic"

            self.te["runs_install_local_software"] += self.setup_local_software_direct(_, env_vars)

        """
        Step 4) Setup other nice variables
        """

        os_short = self.te["os"]
        if os_short == "ubuntu":
            os_short = "ubu"
        elif os_short == "debian":
            os_short = "deb"
        elif os_short == "opensuse":
            os_short = "suse"
        elif os_short == "fedora":
            os_short = "fed"
        elif os_short == "mac":
            os_short = "mac"
        else:
            raise Exception("TODO")

        self.te["id"] = f"{os_short}{self.te['os_version']}-{self.te['compiler']}{self.te['compiler_version']}"

        # Docker image ID used by CI
        self.te["docker_image_id"] = f"sweet-{self.te['id']}"

        # Image ID for local Docker test scripts which are directly executed
        # We need this / in it which doesn"t work on the CI
        self.te["docker_image_id_slash"] = f"sweet/{self.te['id']}"



    def setup_gen_ci_tests(self):
        """
        Generate CI SWEET tests
        """

        #self.te["gen_ci_tests"] = "compile"
        self.te["gen_ci_tests"] = None

        if self.te["os"] == "ubuntu":
            if self.te["compiler"] == "gcc":
                if self.te["os_version"] == 18 and self.te["compiler_version"] == 7:
                    self.te["gen_ci_tests"] = "all"
                elif self.te["os_version"] == 20 and self.te["compiler_version"] == 8:
                    self.te["gen_ci_tests"] = "all"
                elif self.te["os_version"] == 20 and self.te["compiler_version"] == 10:
                    self.te["gen_ci_tests"] = "all"
                elif self.te["os_version"] == 22 and self.te["compiler_version"] == 12:
                    self.te["gen_ci_tests"] = "all"

            elif self.te["compiler"] == "llvm":
                if self.te["os_version"] == 22 and self.te["compiler_version"] == 15:
                    self.te["gen_ci_tests"] = "all"

            else:
                raise Exception("TODO")

        elif self.te["os"] == "debian":
            if self.te["compiler"] == "gcc":
                #if self.te["os_version"] == 10 and self.te["compiler_version"] == 8:
                #    self.te["gen_ci_tests"] = "all"
                #elif self.te["os_version"] == 11 and self.te["compiler_version"] == 10:
                #    self.te["gen_ci_tests"] = "all"
                if self.te["os_version"] == 12 and self.te["compiler_version"] == 12:
                    self.te["gen_ci_tests"] = "all"

        elif self.te["os"] == "opensuse":
            if self.te["compiler"] == "gcc":
                #if self.te["os_version"] == 15.3 and self.te["compiler_version"] == 7:
                #    self.te["gen_ci_tests"] = "all"
                #elif self.te["os_version"] == 15.4 and self.te["compiler_version"] == 11:
                #    self.te["gen_ci_tests"] = "all"
                if self.te["os_version"] == 15.5 and self.te["compiler_version"] == 12:
                    self.te["gen_ci_tests"] = "all"

        elif self.te["os"] == "fedora":
            if self.te["compiler"] == "gcc":
                #if self.te["os_version"] in [36, 37, 38, 39]:
                #if self.te["os_version"] in [36, 39]:
                if self.te["os_version"] in [39] and self.te["compiler_version"] == 13:
                    self.te["gen_ci_tests"] = "all"

        elif self.te["os"] == "mac":
            if self.te["compiler"] == "gcc":
                if self.te["os_version"] == 13:
                    self.te["gen_ci_tests"] = "all"

        else:
            raise Exception("TODO")



# Choose ubuntu and old compiler version
particular_version = False

os_ = ["ubuntu", "opensuse", "debian", "fedora"]
#os_ = ["ubuntu", "opensuse", "debian", "mac"]
#os_ = ["ubuntu"]
#os_ = ["opensuse"]
#os_ = ["fedora"]
#os_ = ["debian"]

if particular_version:
    os_ = ["ubuntu"]


for op_sys in os_:

    # Start with empty dictionary
    te = TestEnvironment()

    te.setup_gitlab_ci_kind(gitlab_ci_kind)

    if op_sys == "ubuntu":
        os_version_ = [18, 20, 22]
        if particular_version:
            os_version_ = [18]

    elif op_sys == "opensuse":
        # https://en.opensuse.org/Lifetime
        #os_version_ = [15.3, 15.4, 15.5]
        os_version_ = [15.5]
    elif op_sys == "debian":
        #os_version_ = [10, 11, 12]
        os_version_ = [12]
    elif op_sys == "fedora":
        #os_version_ = [36, 37, 38, 39]
        os_version_ = [39]
    elif op_sys == "mac":
        os_version_ = [11]
    else:
        raise Exception("TODO")

    os_version_.reverse()

    for os_version in os_version_:
        os_te = te.copy()

        os_te.setup_os(op_sys, os_version)

        if op_sys == "ubuntu":
            compiler_ = ["llvm", "gcc"]
            #compiler_ = ["llvm"]
            if particular_version:
                compiler_ = ["gcc"]

        elif op_sys == "opensuse":
            compiler_ = ["gcc"]
        elif op_sys == "debian":
            compiler_ = ["gcc"]
        elif op_sys == "fedora":
            compiler_ = ["gcc"]
        elif op_sys == "mac":
            compiler_ = ["gcc"]
        else:
            raise Exception("TODO")

        for compiler in compiler_:
            if op_sys == "ubuntu":
                if compiler == "gcc":

                    # Available Ubuntu compiler versions
                    if os_version == 18:
                        compiler_version_ = [7,8]
                    elif os_version == 20:
                        compiler_version_ = [8,9,10]
                    elif os_version == 22:
                        compiler_version_ = [9,10,11,12]
                    else:
                        raise Exception("TODO")

                    if particular_version:
                        compiler_version_ = [12]

                elif compiler == "llvm":
                    # Available Ubuntu compiler versions
                    if os_version == 18:
                        #compiler_version_ = [15]
                        compiler_version_ = []
                    elif os_version == 20:
                        #compiler_version_ = [15]
                        compiler_version_ = []
                    elif os_version == 22:
                        compiler_version_ = [15]
                    else:
                        raise Exception("TODO")
                else:
                    raise Exception("TODO")

            elif op_sys == "debian":
                if compiler == "gcc":

                    # Available Ubuntu compiler versions
                    if os_version == 10:
                        compiler_version_ = [7, 8]
                    elif os_version == 11:
                        compiler_version_ = [9, 10]
                    elif os_version == 12:
                        #compiler_version_ = [11, 12]
                        compiler_version_ = [12]
                    else:
                        raise Exception("TODO")

                else:
                    raise Exception("TODO")

            elif op_sys == "opensuse":
                if compiler == "gcc":
                    if os_version in [15.3, 15.4, 15.5]:
                        #compiler_version_ = [7,9,10,12]
                        compiler_version_ = [12]
                    else:
                        raise Exception("TODO")
                else:
                    raise Exception("TODO")

            elif op_sys == "fedora":
                if compiler == "gcc":

                    # Available Ubuntu compiler versions
                    if os_version == 35:
                        compiler_version_ = [11]
                    elif os_version == 36:
                        compiler_version_ = [12]
                    elif os_version == 37:
                        compiler_version_ = [12]
                    elif os_version == 38:
                        compiler_version_ = [13]
                    elif os_version == 39:
                        compiler_version_ = [13]
                    else:
                        raise Exception("TODO")

                else:
                    raise Exception("TODO")

            elif op_sys == "mac":
                if compiler == "gcc":

                    # Available Ubuntu compiler versions
                    if os_version == 13:
                        compiler_version_ = [12]
                    else:
                        raise Exception("TODO")

                else:
                    raise Exception("TODO")

            else:
                raise Exception("TODO")

            compiler_version_.reverse()

            for compiler_version in compiler_version_:
                te_final = os_te.copy()

                if True:
                    te_final.setup_local_software()

                    te_final.setup_compiler(compiler, compiler_version)

                    te_final.setup_gen_ci_tests()
                    te_final.setup_job_tags()

                    te_final.setup_finalize()

                    test_environment_ += [te_final.te]


if particular_version:
    for te in test_environment_:
        te["gen_ci_tests"] = "all"


