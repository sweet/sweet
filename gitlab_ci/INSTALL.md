
# INRIA CI runners

## Create runners

* Go to to ```https://ci.inria.fr/```
* -> Dashboard
* -> Manage project
* -> Slaves
* -> Add slave
* [configure slave here]<br />
  E.g., with name ```sweet-runner-large'''


## Connect to slave

Click 'connect' for more information:

* ```ssh [username]@ci-ssh.inria.fr```
* ```ssh [name of slave]```<br />
  E.g., ```ssh ci@sweet-runner-large```


### System update

```
apt update
apt upgrade
```

### Install Docker

```
apt install docker.io
```

### Install Gitlab runner on slave

Follow instructions at https://ci.inria.fr/doc/page/gitlab/

```
sudo apt install docker docker.io
sudo wget -O /usr/local/bin/gitlab-runner https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-amd64
sudo chmod +x /usr/local/bin/gitlab-runner
sudo gitlab-runner install --user=ci --working-directory=/builds
sudo gitlab-runner start
sudo gitlab-runner status # should return "service is running"
```

Next, we register the Gitlab

Open website<br />
```https://gitlab.inria.fr/mschreib/sweet-ci-tests/-/settings/ci_cd```<br />
for information about gitlab runners.

Call
```
sudo gitlab-runner register 
```
and use previous information to register gitlab runner.

Example:
```
ci@ci:~$ sudo gitlab-runner register
Runtime platform                                    arch=amd64 os=linux pid=1317 revision=6d480948 version=15.7.1
Running in system-mode.                            
                                                   
Enter the GitLab instance URL (for example, https://gitlab.com/):
https://gitlab.inria.fr/
Enter the registration token:
YOUR TOKEN FROM THE WEBSITE ABOVE COMES HERE
Enter a description for the runner:
[ci]: 
Enter tags for the runner (comma-separated):
linux,ci.inria.fr,very-large,large
Enter optional maintenance note for the runner:

Registering runner... succeeded                     runner=GR1348941vyS2qyxz
Enter an executor: custom, docker-ssh, ssh, kubernetes, docker, parallels, shell, virtualbox, docker+machine, docker-ssh+machine, instance:
docker
Enter the default Docker image (for example, ruby:2.7):
ruby:2.7
Runner registered successfully. Feel free to start it, but if it's running already the config should be automatically reloaded!
 
Configuration (with the authentication token) was saved in "/etc/gitlab-runner/config.toml" 
```

### Reboot & cleanup
Disable caching of images (workaround to avoid accumulation of pulled images):

In [runners.docker], use
```
disable_cache = true
```

### Reboot & cleanup

```
sudo shutdown -r now
sudo apt update
sudo apt upgrade
```

