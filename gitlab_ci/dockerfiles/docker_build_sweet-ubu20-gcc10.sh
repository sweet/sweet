#! /bin/bash

SCRIPTDIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

if [ "$(hostname)" == "time-x" ]; then
    # Use proxy on time-x
    #PROXY="--build-arg https_proxy=http://proxy.in.tum.de:8080/ --build-arg http_proxy=http://proxy.in.tum.de:8080/"
    echo "X"
fi

docker build -t sweet/ubu20-gcc10 ${PROXY} "${SCRIPTDIR}/sweet-ubu20-gcc10" $@
