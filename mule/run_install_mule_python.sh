#! /bin/bash

set -e

python3 -m pip install -e ./python --no-cache-dir

pip install -r requirements.txt

