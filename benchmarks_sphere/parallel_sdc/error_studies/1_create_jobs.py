#! /usr/bin/env python3

import os
import sys
import math
import copy
from itertools import product

from mule.JobGeneration import JobGeneration
from mule.JobParallelizationDimOptions import JobParallelizationDimOptions
from mule.sdc import getSDCSetup


jgbase = JobGeneration()
verbose = True

jgbase.runtime.paramsSDC = getSDCSetup(
    nNodes=3,
    nIter=3,
    nodeType="RADAU-RIGHT", 
    qDeltaImplicit="OPT-QMQD-0", 
    qDeltaExplicit="PIC", 
    qDeltaInitial="BEPAR",
    preSweep = "QDELTA",
    postSweep = "LASTNODE"
)

jgbase.compile.mode = "release"

#
# Mode and Physical resolution
#
jgbase.runtime.space_res_spectral = 128
jgbase.runtime.space_res_grid = -1

jgbase.parallelization.core_oversubscription = False
jgbase.parallelization.core_affinity = "compact"

jgbase.compile.threading = "omp"
jgbase.compile.rexi_thread_parallel_sum = "disable"

gen_reference_solution = False
jgbase.runtime.benchmark_name = "galewsky"

jgbase.runtime.max_simulation_time = 60*60*24*1    # 1 day

jgbase.runtime.output_timestep_size = jgbase.runtime.max_simulation_time
jgbase.runtime.output_file_mode = "bin"

params_timestep_size_reference = 30.0
base_timestep_size = 128/jgbase.runtime.space_res_spectral*1200.0

params_pspace_num_cores_per_rank = [jgbase.platform_resources.num_cores_per_socket]
params_pspace_num_threads_per_rank = [jgbase.platform_resources.num_cores_per_socket]



"""
Time step sizes
"""
ts = []
ts += [2*i for i in range(1, 6)]
ts += [20*i for i in range(1, 6)]
ts += [200*i for i in range(1, 6)]

# Resolution specific multiplier
resmul = 128/jgbase.runtime.space_res_spectral

params_ts_explicit = list(filter(lambda dt: 10*resmul <= dt and dt <= 200*resmul, ts))
#params_ts_implicit = list(filter(lambda dt: 20*resmul <= dt and dt <= 1000*resmul, ts))
params_ts_implicit = list(filter(lambda dt: 10*resmul <= dt and dt <= 2000*resmul, ts))
#params_ts_exponential = list(filter(lambda dt: 20*resmul <= dt and dt <= 2000*resmul, ts))
params_ts_exponential = list(filter(lambda dt: 10*resmul <= dt and dt <= 2000*resmul, ts))

unique_id_filter = []
unique_id_filter.append("compile")
unique_id_filter.append("runtime.max_simulation_time")

jgbase.unique_id_filter = unique_id_filter

def estimateWallclockTime(p):
    return 12*60*60

jgbase.compilecommand_in_jobscript = False


#
# Run simulation on cart2d or sphere
#
jgbase.compile.program = "programs/pde_sweSphere"

jgbase.compile.cart2d_spectral_space = "disable"
jgbase.compile.cart2d_spectral_dealiasing = "disable"
jgbase.compile.sphere2d_spectral_space = "enable"
jgbase.compile.sphere2d_spectral_dealiasing = "enable"

jgbase.compile.benchmark_timings = "enable"
jgbase.compile.quadmath = "disable"

jgbase.runtime.verbosity = 0

# Leave instability checks activated
jgbase.runtime.instability_checks = 0
# Don"t activate them for wallclock time studies since they are pretty costly!!!
#jgbase.runtime.instability_checks = 0

jgbase.runtime.viscosity = 0.0



"""
Reference method
"""
ref_ts = 10*resmul
ref_method = "ERK(ln,order=4)"

o="order=2"
_tsm = [
        f"ERK(ln,o)",
]


#
# Reference solution
#
jgbase.reference_job_unique_id = None



"""
SHTNS plan generation scripts
"""
if True:
    jgplan = copy.deepcopy(jgbase)

    """
    Search for plans and store them
    This is in particular important for running studies across several nodes
    since they rely on using the same transformation plans in order to have no
    load imbalances
    """

    jgplan.runtime.reuse_plans = "save"

    setupParallelization(jgplan)

    # Dummy values
    jgplan.runtime.timestep_size  = ref_ts
    jgplan.runtime.timestepping_method = ref_method
    jgplan.runtime.max_simulation_time = 10*ref_ts    # 10 time steps

    # Skip postprocessing, otherwise an error will show up
    jgplan.skip_postprocessing = True

    jgplan.gen_jobscript_directory("job_shplans_"+jgplan.getUniqueID())

    compileCommandsAccum(jgplan)



"""
Reference job
"""
if True:
    jgref = copy.deepcopy(jgbase)

    jgref.runtime.timestep_size  = ref_ts
    jgref.runtime.timestepping_method = ref_method

    setupParallelization(jgref)

    jgref.reference_job = True
    jgref.parallelization.max_wallclock_seconds = 4*60*60

    jgref.gen_jobscript_directory("job_bench_reference_"+jgref.getUniqueID())
    compileCommandsAccum(jgref)

    # Set the unique ID in the base job!
    jgbase.reference_job_unique_id = jgref.job_unique_id



if True:
    """
    Create benchmarks
    """
    # Load REXI benchmarks
    rb = rexi_benchmarks.getREXIBenchmarks()

    for tsm in _tsm:
        jg = copy.deepcopy(jgbase)

        jg.runtime.timestepping_method = tsm

        if "exp" in tsm.lower() or "rexi" in tsm.lower() or "etdrk" in tsm.lower():
            params_ts = params_ts_exponential
        elif "irk" in tsm.lower():
            params_ts = params_ts_implicit
        elif "erk" in tsm.lower():
            params_ts = params_ts_explicit
        else:
            raise Exception(f"Unhandled time stepping method '{tsm}'")


        for ts in params_ts:

            jg.runtime.timestep_size = ts

            if "rexi" not in tsm.lower():
                """
                Regular job
                """
                setupParallelization(jg)
                jg.parallelization.max_wallclock_seconds = estimateWallclockTime(jg)

                jg.gen_jobscript_directory("job_bench_"+jg.getUniqueID())
                compileCommandsAccum(jg)
                continue

            """
            REXI job
            """
            for r in rb:
                jg.runtime.rexi_method = r["rexi_method"]
                jg.runtime.rexi_files_coefficients = r["rexi_files_coefficients"]

                setupParallelization(jg, jg.runtime.rexi_files_coefficients[0].len())

                jg.parallelization.max_wallclock_seconds = estimateWallclockTime(jg)

                jg.gen_jobscript_directory("job_bench_"+jg.getUniqueID())
                compileCommandsAccum(jg)


jg.write_compilecommands(content_accum=compile_commands_accum)


