#! /usr/bin/env python3
import sys
from mule.sdc.testing import checkConvergence

plotLim = 1e-10, 1e1
plotLim = None          # Comment to use the limits above
checkConvergence(generatePlot=True, plotLim=plotLim, checkOrder=False)

if len(sys.argv) <= 1:
    print("*"*80)
    print("Convergence tests successful")
    print("*"*80)