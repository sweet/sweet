#! /usr/bin/env python3
import numpy as np
from mule.sdc import getSDCSetup
from mule.sdc.testing import setupDahlquistBenchmark, setupSDCConvergenceRuns

lamI = 0.0j
lamE = 0.0j
mu = 1
phi = 2*np.pi*1j
tEnd = np.pi + np.pi/2

nStepMin = 4        # minimum number of time-steps per period
nRefinement = 10      # number of time-step refinements
dtSizes = [2*np.pi/(nStepMin*2**i) for i in range(nRefinement+1)]
print("Time steps :", dtSizes)

# Setup Dahlquist benchmark and reference simulation
jg, terms = setupDahlquistBenchmark(tEnd, lamI, lamE, mu, phi, fExplicit=False)

# Setup SDC runs
baseParams = dict(
    nNodes=2,
    nIter=None,
    nodeType="RADAU-RIGHT",
    # qDeltaImplicit=["DNODES-3", "DNODES-4"],
    qDeltaImplicit="BE",
    qDeltaExplicit="PIC",
    preSweep="QDELTA",
    qDeltaInitial="BE",
    # qDeltaImplicit="BE",
    # qDeltaExplicit="FE",
    # preSweep="QDELTA",
    # qDeltaInitial="BE",
    postSweep="LASTNODE",
    nodeDistr="EQUID",
)

for nIter in [0]:
    params = baseParams.copy()
    params.update(nIter=nIter)
    params = getSDCSetup(**params)
    setupSDCConvergenceRuns(jg, terms, dtSizes, params, formulation="FP")
