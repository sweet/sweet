#!/usr/bin/env python
import numpy as np
import pandas as pd

from mule.postprocessing.JobsData import JobsData
from mule.postprocessing.sphere2D import Solution

print('Extracting jobs')
runs = JobsData(verbosity=0, useUniqueId=False)

results = {}
for jobName in runs.jobNames:

    method, dt = jobName.split('_dt')
    dt = float(dt)

    solution = Solution(runs[jobName])

    times = solution.times
    solution.load(solution.times[-1])

    print(f'Computing spectrum for {method}, dt={dt}')
    spectrum, wavelengths = solution.computeKineticEnergySpectrum()

    series = pd.Series(index=wavelengths, data=spectrum)
    series.index.name = 'wavelength'
    series.name = 'spectrum'
    series.to_markdown(f'{jobName}/spectrum.md')

    # Compute stability criterion
    if np.any(np.isnan(spectrum)):
        stable = False
    else:
        stable = (np.mean(spectrum[-20:]) < np.mean(spectrum[-40:-20])) \
            and np.max(spectrum) < 1e3
    print(" -- stable", stable)

    # Save stability results
    if method not in results:
        results[method] = {}
    results[method][dt] = stable

df = pd.DataFrame(results)


df.sort_index(inplace=True) # Sort by time-steps
df = df.T                   # transpose
df.index.name = "timeStepping"
df.sort_index(inplace=True) # Sort by time-stepping method

df.to_markdown('stabilityResults.md')
