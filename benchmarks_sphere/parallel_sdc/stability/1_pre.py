#!/usr/bin/env python
import itertools

from mule.simu.galwesky import GalewskyBenchmark
from mule.sdc import getParamsSDC

nModes = 512
nSteps = 10
dtVals = range(1, 37)

SDC_VARIANTS = {
    "nNodes": [4],
    "nodeDistr": ["LEGENDRE", "EQUID"],
    "preSweep": ["COPY", "QDELTA"],
    "nodeType": ["LOBATTO", "RADAU-RIGHT"],
    "nIter": [1, 2, 3]
}
SDC_PARAMS = []
keys = list(SDC_VARIANTS.keys())
for conf in itertools.product(*SDC_VARIANTS.values()):
    conf = list(conf)
    if conf[3] == 'LOBATTO':
        conf[0] += 1
    SDC_PARAMS.append({k: v for k, v in zip(keys, conf)})

SDC_SWEEPS = [
    # (Initial sweep (implicit), implicit sweep, explicit sweep)
    ('BE', 'BE', 'FE'),
    ('BE', 'BE', 'PIC'),
    ('BEPAR', 'BEPAR', 'PIC'),
    ('TRAPAR', 'TRAPAR', 'PIC'),
    ('BEPAR', 'MIN-SR-NS', 'PIC'),
    ('BEPAR', 'MIN-SR-S', 'PIC'),
    ('BEPAR', [f'DNODES-{i+1}' for i in range(3)], 'PIC'),
]


for dt in dtVals:

    dt *= 128/nModes*100
    tEnd = nSteps*dt

    simu = GalewskyBenchmark(verbose=True)
    simu.setSimulation(tEnd=tEnd)
    simu.setOutput(dt=tEnd)
    simu.setBenchmark(nModes=nModes, nProcSpace=8)

    # ERK methods
    for order in [1,2,3,4]:
        method = f"ERK(ln,o={order})"
        simu.setTimeStepping(method, dt)
        simu.writeJobDir(dirName=f"ERK{order}_dt{int(dt):003d}")

    # Classic IMEX SDC
    for params in SDC_PARAMS:
        method = "SDC-FP(i=lg,ADDT(lc,n))"
        for p1, p2, p3 in SDC_SWEEPS:
            paramsSDC = getParamsSDC(
                qDeltaInitial=p1, qDeltaImplicit=p2, qDeltaExplicit=p3,
                **params)
            simu.setTimeStepping(method, dt, paramsSDC=paramsSDC)
            simu.writeJobDir(dirName=f"SDC_{paramsSDC['idString']}_dt{int(dt):003d}")
