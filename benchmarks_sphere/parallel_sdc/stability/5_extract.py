#!/usr/bin/env python
import io
import pandas as pd

def readMarkdownTable(file):

    # Read markdown table in file
    with open(file) as f:
        data = f.read().strip()

    # Convert into pandas dataframe
    df = pd.read_table(
        io.StringIO(data), sep="|", header=0, index_col=0,
        skipinitialspace=True).dropna(axis=1, how='all').iloc[1:]
    df.reset_index(inplace=True, drop=True)
    df.columns = [label.strip() for label in df.columns]
    df = df.map(lambda x: str(x).strip())

    return df


df = readMarkdownTable("stabilityResults.md")
df["timeStepping"] = df["timeStepping"].str[10:]
df.set_index("timeStepping", inplace=True)
df = df.astype(int)

stab = pd.DataFrame(
    index=df.index,
    data=df.columns[df.values.argmin(axis=1)-1]).astype(float)
stab.columns = ["dtMax"]


def extract(sID):
    # ERK methods
    if sID.startswith('ERK'):
        return [int(sID[-1:]), False]
    # SDC methods
    _, nNodes, qType, _, nSweeps, _, explSweep, initSweep, _ = \
        sID.split('_')
    nNodes = int(nNodes[1:])
    nSweeps = int(nSweeps[1:])
    nStagePerIter = nNodes - 1 if qType == "LOBATTO" else nNodes
    nIter = nSweeps if initSweep == "COPY" else nSweeps+1
    return [nIter*nStagePerIter, explSweep=="PIC"]

stab[["nStages", "parallel"]] = [extract(sID) for sID in stab.index]
stab["dtMaxEff"] = stab["dtMax"] / stab["nStages"]
stab["dtMaxEffPar"] = stab["dtMax"] / stab["nStages"]
stab.loc[stab.parallel, "dtMaxEffPar"] *= 4

stab.sort_values(by="dtMaxEffPar", ascending=False, inplace=True)
