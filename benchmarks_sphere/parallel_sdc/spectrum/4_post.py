import numpy as np
import matplotlib.pyplot as plt

from mule.postprocessing.JobsData import JobsData
from mule.postprocessing.sphere2D import Solution


print('Extracting file names')
job = JobsData(verbosity=0)[0]
solution = Solution(job)

print('Reading one field')
d = float(24*60*60)
h = float(60*60)
time = 14*d



solution.load(time)

hours = time/h
days = time/d
label = f'{days:.1f} day{"s" if days > 1 else ""}'
# label = f'{hours:.0f} hour{"s" if hours > 1 else ""}'

if True:
    profile = solution.get('vrt', spectral=False, time=0).T[0]
    scaling = np.max(np.abs(profile))
    nModes = profile.size

    field = solution.get('vrt', spectral=False, time=time)

    print('Plotting one field')
    plt.figure()
    numrows, numcols = field.shape
    extent = (-0.5, numcols-0.5, -0.5, numrows-0.5)
    plt.imshow(field, extent=extent)
    plt.colorbar()

    diff = (field.T-profile)
    diff /= scaling
    l2diff = np.linalg.norm(diff, axis=-1, ord=np.inf)
    tInd = np.max(l2diff)
    print('transition indicator :', tInd)

    if False:
        indicator = []
        for n in range(169):
            print(f'extracting indicator after {n} hour(s)')
            field = solution.get('vrt', spectral=False, time=n*h)
            diff = (field.T-profile)
            diff /= scaling
            l2diff = np.linalg.norm(diff, axis=-1, ord=np.inf)
            tInd = np.max(l2diff)
            indicator.append(tInd)
        plt.figure('transition indicator')
        plt.plot(indicator, label=f'M={nModes}')
        plt.legend()
        plt.xlabel('hours')

if True:
    print('Computing spectrum')
    spectrum, wavelengths = solution.computeKineticEnergySpectrum()

    plt.figure('ke spectrum')
    plt.loglog(wavelengths, spectrum, label=label)
    plt.legend()
    plt.grid(True)

    wTh = wavelengths[10:]
    plt.plot(wTh, 5e-11*wTh**(3), '--', c='k')

    wTh = wavelengths[100:]
    # plt.plot(wTh, 2e-6*wTh**(5/3), '--', c='k')

    xlim = plt.xlim()
    plt.xlim(max(xlim), min(xlim))

    plt.xlabel("Wavelength (km)")
    plt.ylabel("Amplitude")
    plt.tight_layout()
