
from mule.sdc import getSDCSetup

paramsSDC_ = []

print("*"*80)
print("* Preparing pSDC parameters")
print("*"*80)
for nIter in [1, 2, 3]:
#for nIter in [1]:
    #for qDeltaImplicit in ["BEPAR", "MIN-SR-S", "MIN-SR-NS"]:
    for qDeltaImplicit in ["BEPAR"]:
        print("...")
        paramsSDC_ += [
            getSDCSetup(
                nNodes = 4,
                nIter = nIter,
                nodeType = "RADAU-RIGHT", 
                qDeltaInitial = "BEPAR",
                qDeltaExplicit = "PIC", 
                qDeltaImplicit = qDeltaImplicit,
                preSweep = "QDELTA",
                postSweep = "LASTNODE"
            )
        ]

