#! /usr/bin/env python3

import os
import sys
import math
import copy
from itertools import product

from mule.JobGeneration import JobGeneration
from mule.JobParallelizationDimOptions import JobParallelizationDimOptions


jgbase = JobGeneration()
verbose = True

jgbase.compile.mode = "release"

#
# Mode and Physical resolution
#
jgbase.runtime.space_res_spectral = 128
jgbase.runtime.space_res_grid = -1

jgbase.parallelization.core_oversubscription = False
jgbase.parallelization.core_affinity = "compact"

jgbase.compile.threading = "omp"
jgbase.compile.rexi_thread_parallel_sum = "disable"

gen_reference_solution = False
jgbase.runtime.benchmark_name = "galewsky"

jgbase.runtime.max_simulation_time = 60*60*24*1    # 1 day

jgbase.runtime.output_timestep_size = jgbase.runtime.max_simulation_time
jgbase.runtime.output_file_mode = "bin"

params_timestep_size_reference = 30.0
base_timestep_size = 128/jgbase.runtime.space_res_spectral*1200.0

params_pspace_num_cores_per_rank = [jgbase.platform_resources.num_cores_per_socket]
params_pspace_num_threads_per_rank = [jgbase.platform_resources.num_cores_per_socket]

jgbase.parallelization.max_wallclock_seconds = 60*60*1  # Limit to one hour


"""
Time step sizes
"""
ts = []
ts += [2*i for i in range(1, 6)]
ts += [20*i for i in range(1, 6)]
ts += [200*i for i in range(1, 6)]
ts += [2000*i for i in range(1, 6)]

# Resolution specific multiplier
resmul = 128/jgbase.runtime.space_res_spectral

params_ts_explicit = list(filter(lambda dt: 10*resmul <= dt and dt <= 200*resmul, ts))
#params_ts_implicit = list(filter(lambda dt: 20*resmul <= dt and dt <= 1000*resmul, ts))
params_ts_implicit = list(filter(lambda dt: 10*resmul <= dt and dt <= 2000*resmul, ts))
params_ts_sdc = list(filter(lambda dt: 10*resmul <= dt and dt <= 1000*resmul, ts))
#params_ts_exponential = list(filter(lambda dt: 20*resmul <= dt and dt <= 2000*resmul, ts))
params_ts_exponential = list(filter(lambda dt: 10*resmul <= dt and dt <= 2000*resmul, ts))

unique_id_filter = []
unique_id_filter.append("compile")
unique_id_filter.append("runtime.max_simulation_time")
unique_id_filter.append("runtime.benchmark")

jgbase.unique_id_filter = unique_id_filter

jgbase.compilecommand_in_jobscript = False

compile_commands_accum = []
def compileCommandsAccum(jg):
    for i in jg.get_compilecommands_accum():
        if i not in compile_commands_accum:
            compile_commands_accum.append(i)

#
# Run simulation on cart2d or sphere
#
jgbase.compile.program = "programs/PDE_SWESphere2D"

jgbase.compile.cart2d_spectral_space = "disable"
jgbase.compile.cart2d_spectral_dealiasing = "disable"
jgbase.compile.sphere2d_spectral_space = "enable"
jgbase.compile.sphere2d_spectral_dealiasing = "enable"

jgbase.runtime.verbosity = 0

# Leave instability checks activated
jgbase.runtime.instability_checks = 0
# Don"t activate them for wallclock time studies since they are pretty costly!!!
#jgbase.runtime.instability_checks = 0

jgbase.runtime.viscosity = 0.0



"""
Reference method
"""
ref_ts = 10*resmul
ref_method = "ERK(ln,order=4)"

o="order=2"
_tsm = [
        #f"ERK(ln,{o})",
        f"SDC-FP(l,n)",
]

def setupParallelizationSDC(jg, ranks_in_time=None, threads_in_time=None):


    pspace = JobParallelizationDimOptions("space")
    pspace.num_cores_per_rank = jgbase.platform_resources.num_cores_per_socket
    pspace.num_threads_per_rank = jgbase.platform_resources.num_cores_per_socket
    pspace.num_ranks = 1
    #pspace.threading = "omp"

    jg.runtime.sh_setup_num_threads = pspace.num_threads_per_rank
    jg.parallelization.withNestedOpenMPNumThreads = False

    if ranks_in_time is not None or threads_in_time is not None:
        # Update TIME parallelization
        ptime = JobParallelizationDimOptions("time")
        ptime.num_cores_per_rank = threads_in_time
        ptime.num_threads_per_rank = threads_in_time
        ptime.num_ranks = ranks_in_time
        #ptime.threading = "omp"

        ptime.print()

        jg.setup_parallelization([ptime, pspace])

    else:
        jg.setup_parallelization([pspace])

    if verbose:
        print("Parallelization output:")
        jg.parallelization.print()


#
# Reference solution
#
jgbase.reference_job_unique_id = None



"""
SHTNS plan generation scripts
"""
if True:
    jgplan = copy.deepcopy(jgbase)

    """
    Search for plans and store them
    This is in particular important for running studies across several nodes
    since they rely on using the same transformation plans in order to have no
    load imbalances
    """

    jgplan.runtime.reuse_plans = "save"

    setupParallelizationSDC(jgplan)

    # Dummy values
    jgplan.runtime.timestep_size  = ref_ts
    jgplan.runtime.timestepping_method = ref_method
    jgplan.runtime.max_simulation_time = 10*ref_ts    # 10 time steps

    # Skip postprocessing, otherwise an error will show up
    jgplan.skip_postprocessing = True

    jgplan.gen_jobscript_directory("job_shplans_"+jgplan.getUniqueID())

    compileCommandsAccum(jgplan)


"""
Reference job
"""
if True:
    jgref = copy.deepcopy(jgbase)

    jgref.runtime.timestep_size  = ref_ts
    jgref.runtime.timestepping_method = ref_method

    setupParallelizationSDC(jgref)

    jgref.reference_job = True
    jgref.parallelization.max_wallclock_seconds = 4*60*60

    jgref.gen_jobscript_directory("job_bench_reference_"+jgref.getUniqueID())
    compileCommandsAccum(jgref)

    # Set the unique ID in the base job!
    jgbase.reference_job_unique_id = jgref.job_unique_id



from benchmark_sdc_params import paramsSDC_


if True:
    """
    Create benchmarks
    """

    for tsm in _tsm:
        jg = copy.deepcopy(jgbase)

        jg.runtime.timestepping_method = tsm

        if "exp" in tsm.lower() or "rexi" in tsm.lower() or "etdrk" in tsm.lower():
            params_ts = params_ts_exponential
        elif "irk" in tsm.lower():
            params_ts = params_ts_implicit
        elif "sdc" in tsm.lower():
            params_ts = params_ts_sdc
        elif "erk" in tsm.lower():
            params_ts = params_ts_explicit
        else:
            raise Exception(f"Unhandled time stepping method '{tsm}'")

        for ts in params_ts:
            jg.runtime.timestep_size = ts

            if "SDC" not in jg.runtime.timestepping_method:

                setupParallelizationSDC(jg)

                jg.gen_jobscript_directory("job_bench_"+jg.getUniqueID())
                compileCommandsAccum(jg)

            else:
                for paramsSDC in paramsSDC_:

                    for sdc_parallel in [0, 1]:

                        jgsdc = copy.deepcopy(jg)

                        if sdc_parallel == 1:
                            jgsdc.compile.parallel_sdc_par_model = "omp"

                        # We first setup all parameters
                        jgsdc.runtime.sdc_params = paramsSDC
                        jgsdc.runtime.sdc_parallel = sdc_parallel
                        setupParallelizationSDC(jgsdc, 1, 4)

                        # Ask for jobID (including all parameters)
                        jobdir = "job_bench_"+jgsdc.getUniqueID()

                        # Set the SDC file
                        jgsdc.runtime.sdc_file = "params_SDC.sweet"

                        jgsdc.gen_jobscript_directory(jobdir)

                        # Write out SDC coefficients after job directory has been created
                        paramsSDC.writeToFile(jgsdc.job_dirpath+"/"+jgsdc.runtime.sdc_file)

                        compileCommandsAccum(jgsdc)




jg.write_compilecommands(content_accum=compile_commands_accum)


