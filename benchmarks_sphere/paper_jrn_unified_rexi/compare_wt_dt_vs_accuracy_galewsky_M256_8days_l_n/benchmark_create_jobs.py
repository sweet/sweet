#! /usr/bin/env python3

import os
import sys
import math
import copy

from mule.JobGeneration import *
from mule.JobParallelization import *
from mule.JobParallelizationDimOptions import *

# REXI
import rexi_benchmarks


verbose = False
#verbose = True


##################################################
##################################################
##################################################

jgbase = JobGeneration()

"""
Compile parameters
"""
jgbase.compile.mode = "release"
jgbase.compile.sweet_mpi = "enable"
jgbase.compile.threading = "omp"
# We only use MPI for REXI terms
jgbase.compile.rexi_thread_parallel_sum = "disable"


# Run simulation on cart2d or sphere2d
jgbase.compile.program = "programs/PDE_SWESphere2D"
jgbase.compile.benchmark_timings = "enable"



"""
Runtime parameters
"""
days = 8    # How many days
jgbase.runtime.max_simulation_time = 60*60*24*days
jgbase.runtime.output_timestep_size = jgbase.runtime.max_simulation_time
jgbase.runtime.space_res_spectral = 256
#jgbase.runtime.space_res_spectral = 128
jgbase.runtime.reuse_plans = "require_load"
jgbase.runtime.verbosity = 0
jgbase.runtime.rexi_method = "file"
jgbase.runtime.benchmark_name = "galewsky"
jgbase.runtime.compute_errors = 0
jgbase.runtime.instability_checks = 0


"""
Time step sizes
"""
ts = []
ts += [2*i for i in range(1, 6)]
ts += [20*i for i in range(1, 6)]
ts += [200*i for i in range(1, 6)]

# Resolution specific multiplier
resmul = 128/jgbase.runtime.space_res_spectral

params_ts_explicit = list(filter(lambda dt: 10*resmul <= dt and dt <= 200*resmul, ts))
#params_ts_implicit = list(filter(lambda dt: 20*resmul <= dt and dt <= 1000*resmul, ts))
params_ts_implicit = list(filter(lambda dt: 10*resmul <= dt and dt <= 2000*resmul, ts))
#params_ts_exponential = list(filter(lambda dt: 20*resmul <= dt and dt <= 2000*resmul, ts))
params_ts_exponential = list(filter(lambda dt: 10*resmul <= dt and dt <= 2000*resmul, ts))


"""
Parallelization parameters
"""
jgbase.parallelization.force_turbo_off = True
jgbase.parallelization.core_oversubscription = False
jgbase.parallelization.core_affinity = "compact"


"""
Compile commands
"""
# Request dedicated compile script
jgbase.compilecommand_in_jobscript = False

# Accumulator for compile commands
compile_commands_accum = []
def compileCommandsAccum(jg):
    for i in jg.get_compilecommands_accum():
        if i not in compile_commands_accum:
            compile_commands_accum.append(i)




"""
Filters for job directories to make them look nicer
"""
unique_id_filter = []
unique_id_filter.append("compile")
#unique_id_filter.append("timestep_size")
#unique_id_filter.append("rexi_params")

unique_id_filter.append("runtime.disc_space")
unique_id_filter.append("runtime.reuse_plans")
unique_id_filter.append("runtime.simparams")
unique_id_filter.append("runtime.benchmark")
unique_id_filter.append("runtime.max_wallclock_time")

#unique_id_filter.append("parallelization.mpi_ranks")
unique_id_filter.append("parallelization.cores_per_rank")
unique_id_filter.append("parallelization.threads_per_rank")
#unique_id_filter.append("parallelization.dims")

jgbase.unique_id_filter = unique_id_filter



"""
Reference method
"""
ref_ts = 10*resmul
ref_method = "ERK(ln,order=4)"


# Different time stepping methods to investigate
o="order=2"
_tsm = [
        f"ERK(ln,order=4)",
        f"ERK(ln,order=3)",
        f"ERK(ln,{o})",

        #f"SS(ERK(l,{o}),ERK(n,{o}),{o})",
        #f"SS(ERK(lg,{o}),ERK(ADDT(lc,n),{o}),{o})",

        f"SS(IRK(l,{o}),ERK(n,{o}),{o})",
        f"SS(ERK(n,{o}),IRK(l,{o}),{o})",

        f"ETDRK(REXI(l),n,{o})",

        f"SS(REXI(l),ERK(n,{o}),{o})",
        f"SS(ERK(n,{o}),REXI(l),{o})",

        # Subcycling variants
        f"SS(ERK(n,{o}),ERK(l,order=4),{o})",
        f"SS(ERK(n,{o}),SUBC(ERK(l,order=4),n=2),{o})",
        f"SS(ERK(n,{o}),SUBC(ERK(l,order=4),n=3),{o})",
        f"SS(ERK(n,{o}),SUBC(ERK(l,order=4),n=4),{o})",
    ]


def setupParallelization(jg, ranks_in_time=None):

    pspace = JobParallelizationDimOptions("space")
    pspace.num_cores_per_rank = jgbase.platform_resources.num_cores_per_socket
    pspace.num_threads_per_rank = jgbase.platform_resources.num_cores_per_socket
    pspace.num_ranks = 1

    if ranks_in_time is not None:
        # Update TIME parallelization
        ptime = JobParallelizationDimOptions("time")
        ptime.num_cores_per_rank = 1
        ptime.num_threads_per_rank = 1
        ptime.num_ranks = ranks_in_time
        ptime.num_ranks = min(ptime.num_ranks, jgbase.platform_resources.num_nodes // jgbase.platform_resources.num_sockets_per_node)

        jg.setup_parallelization([pspace, ptime])

    else:
        jg.setup_parallelization([pspace])


    if verbose:
        pspace.print()
        ptime.print()
        jg.parallelization.print()


def estimateWallclockTime(jg):
    return 60*60*1



"""
SHTNS plan generation scripts
"""
if True:
    jgplan = copy.deepcopy(jgbase)

    """
    Search for plans and store them
    This is in particular important for running studies across several nodes
    since they rely on using the same transformation plans in order to have no
    load imbalances
    """

    jgplan.runtime.reuse_plans = "save"

    setupParallelization(jgplan)

    # Dummy values
    jgplan.runtime.timestep_size  = ref_ts
    jgplan.runtime.timestepping_method = ref_method
    jgplan.runtime.max_simulation_time = 10*ref_ts    # 10 time steps

    # Skip postprocessing, otherwise an error will show up
    jgplan.skip_postprocessing = True

    jgplan.gen_jobscript_directory("job_shplans_"+jgplan.getUniqueID())

    compileCommandsAccum(jgplan)



"""
Reference job
"""
if True:
    jgref = copy.deepcopy(jgbase)

    jgref.runtime.timestep_size  = ref_ts
    jgref.runtime.timestepping_method = ref_method

    setupParallelization(jgref)

    jgref.reference_job = True
    jgref.parallelization.max_wallclock_seconds = 4*60*60

    jgref.gen_jobscript_directory("job_bench_reference_"+jgref.getUniqueID())
    compileCommandsAccum(jgref)

    # Set the unique ID in the base job!
    jgbase.reference_job_unique_id = jgref.job_unique_id




if True:
    """
    Create benchmarks
    """
    # Load REXI benchmarks
    rb = rexi_benchmarks.getREXIBenchmarks()

    for tsm in _tsm:
        jg = copy.deepcopy(jgbase)

        jg.runtime.timestepping_method = tsm

        if "exp" in tsm.lower() or "rexi" in tsm.lower() or "etdrk" in tsm.lower():
            params_ts = params_ts_exponential
        elif "irk" in tsm.lower():
            params_ts = params_ts_implicit
        elif "erk" in tsm.lower():
            params_ts = params_ts_explicit
        else:
            raise Exception(f"Unhandled time stepping method '{tsm}'")


        for ts in params_ts:

            jg.runtime.timestep_size = ts

            if "rexi" not in tsm.lower():
                """
                Regular job
                """
                setupParallelization(jg)
                jg.parallelization.max_wallclock_seconds = estimateWallclockTime(jg)

                jg.gen_jobscript_directory("job_bench_"+jg.getUniqueID())
                compileCommandsAccum(jg)
                continue

            """
            REXI job
            """
            for r in rb:
                jg.runtime.rexi_method = r["rexi_method"]
                jg.runtime.rexi_files_coefficients = r["rexi_files_coefficients"]

                setupParallelization(jg, jg.runtime.rexi_files_coefficients[0].len())

                jg.parallelization.max_wallclock_seconds = estimateWallclockTime(jg)

                jg.gen_jobscript_directory("job_bench_"+jg.getUniqueID())
                compileCommandsAccum(jg)


jg.write_compilecommands(content_accum=compile_commands_accum)


