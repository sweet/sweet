#! /usr/bin/env python3

import os
import sys
import math

from itertools import product

# REXI
from mule.rexi.REXICoefficients import *
from mule.rexi.trexi.TREXI import *
from mule.rexi.cirexi.CIREXI import *
from mule.rexi.brexi.BREXI import *

# EFloat
efloat_mode = "float"



def getREXIBenchmarks(jg = None):

    # Accumulator of all REXI methods
    # rexi_method['rexi_method'] = 'file'               # Choose REXI method which is typically 'file' for all file-based ones
    # rexi_method['rexi_files_coefficients'] = None     # List with approximations for different 'phi' functions
    rexi_methods = []



    #
    # CI REXI
    #
    if True:
        # REXI stuff
        def fun_params_ci_N(ci_max_real, ci_max_imag):
            if ci_max_imag >= 7:
                return 128
            else:
                return 32

        params_ci_max_imag = [30.0]
        params_ci_max_real = [10.0]

        #
        # Scale the CI circle radius relative to this time step size
        # We do this simply to get a consistent time stepping method
        # Otherwise, CI would not behave consistently
        # Yes, that's ugly, but simply how it goes :-)
        #
        params_ci_max_imag_scaling_relative_to_timestep_size = 480
        #params_ci_max_imag_scaling_relative_to_timestep_size = None

        params_ci_min_imag = 5.0

        for ci_max_imag, ci_max_real in product(params_ci_max_imag, params_ci_max_real):

            rexi_method = {}
            # Choose REXI method which is typically 'file' for all file-based ones
            rexi_method['rexi_method'] = 'file'
            # List with approximations for different 'phi' functions
            rexi_method['rexi_files_coefficients'] = None

            #if params_ci_max_imag_scaling_relative_to_timestep_size != None:
            #    ci_max_imag *= (jg.runtime.timestep_size/params_ci_max_imag_scaling_relative_to_timestep_size)

            # "phi0"
            cirexi = CIREXI(efloat_mode = efloat_mode)
            coeffs_phi0 = cirexi.setup(function_name="phi0", N=fun_params_ci_N(ci_max_real, ci_max_imag), lambda_include_imag=ci_max_imag, lambda_max_real=ci_max_real).toFloat()
            coeffs_phi0.normalize_steady_state()

            # "phi1"
            cirexi = CIREXI(efloat_mode = efloat_mode)
            coeffs_phi1 = cirexi.setup(function_name="phi1", N=fun_params_ci_N(ci_max_real, ci_max_imag), lambda_include_imag=ci_max_imag, lambda_max_real=ci_max_real).toFloat()
            coeffs_phi1.normalize_steady_state()

            # "phi2"
            cirexi = CIREXI(efloat_mode = efloat_mode)
            coeffs_phi2 = cirexi.setup(function_name="phi2", N=fun_params_ci_N(ci_max_real, ci_max_imag), lambda_include_imag=ci_max_imag, lambda_max_real=ci_max_real).toFloat()
            coeffs_phi2.normalize_steady_state()

            rexi_method['rexi_files_coefficients'] = [coeffs_phi0, coeffs_phi1, coeffs_phi2]

            # Add to list of REXI methods
            rexi_methods.append(rexi_method)


    #
    # B REXI
    #
    if True:
        collocation_type_ = []
        collocation_type_ += ["gauss_legendre"]
#        collocation_type_ += ["gauss_lobatto"]
#        collocation_type_ += ["gauss_chebyshev_t"]
#        collocation_type_ += ["gauss_chebyshev_u"]
#        collocation_type_ += ["equidistant"]

        #_M = [2**i for i in range(0, 6)]
        _M = [2**i for i in range(0, 3)]
        for collocation_type in collocation_type_:
            for M in _M:
                brexi_phi0 = BREXI()

                print(f"{collocation_type}: {M}")

                brexi_phi0.setup(M, collocation_type)

                coeffs_phi0 = brexi_phi0.getCoeffs().toFloat()

                coeffs_phi1 = coeffs_phi0.getNextOrderPhi()
                coeffs_phi2 = coeffs_phi1.getNextOrderPhi()

                rexi_method = {}

                # Choose REXI method which is typically 'file' for all file-based ones
                rexi_method['rexi_method'] = 'file'

                # List with approximations for different 'phi' functions
                rexi_method['rexi_files_coefficients'] = [coeffs_phi0, coeffs_phi1, coeffs_phi2]

                if True:
                    for r in rexi_method['rexi_files_coefficients']:
                        print("")
                        print(f"function_name: {r.function_name}")
                        print(f"gamma: {r.gamma}")
                        print(f"alphas: {r.alphas}")
                        print(f"betas: {r.betas}")

                # Add to list of REXI methods
                rexi_methods.append(rexi_method)

    return rexi_methods


if __name__ == "__main__":
    pass
