#! /bin/bash

rm -rf output_plotting_* job_bench_*


./benchmark_create_jobs.py &&	\
./compile_platform_default_llvm.sh &&	\
mule.benchmark.jobs_run_directly &&	\
./postprocessing.sh  &&	\
evince output_plotting_runtime-timestep-size_vs_sphere2d-data-diff-prog-phi-pert-res-norm-linf_all.pdf

