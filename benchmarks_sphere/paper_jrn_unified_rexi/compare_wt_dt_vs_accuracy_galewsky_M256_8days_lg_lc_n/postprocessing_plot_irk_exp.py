#! /usr/bin/env python3

import sys
import math
import re

from mule.plotting.Plotting import *
from mule.postprocessing.JobsData import *
from mule.postprocessing.JobsDataConsolidate import *
import mule.utils as utils

# For output file naming
postprocessing_kind = "irk_exp"

# Group together  similar time stepping methods
groups = ["runtime.timestepping_method", "runtime.rexi_files_coefficients.0.unique_id_string"]

# Create plots for these variables
vars_ = ["phi_pert", "vrt", "div"]
#vars_ = ["phi_pert"]

# Tagnames used for plotting later on
tagnames_y = []
for i in vars_:
    tagnames_y += [
#        f"sphere2d_data_diff_prog_{i}.res_norm_l1",
#        f"sphere2d_data_diff_prog_{i}.res_norm_l2",
        f"sphere2d_data_diff_prog_{i}.res_norm_linf",
    ]


mule_plotting_usetex(False)




# Load all
jobs_data = JobsData("./job_bench_*", verbosity=0, withReferenceJobs=False)




print("")
print("Groups:")

c = JobsDataConsolidate(jobs_data)
job_groups = c.create_groups(groups)


key_search_replace = []
for var in vars_:
    key_search_replace += [
            [
                # search
                r"sphere2d_data_norms_grid_space_output_prog_"+var+r"_t([0-9\.]{1,}).norm_(.*)",
                # replace
                r"sphere2d_data_diff_prog_"+var+f".res_norm_\\2"
            ]
        ]
JobsData_GroupsRegSub(job_groups, key_search_replace)



"""
The plotting data is now available at known dictionary entires (tagnames_y)

We are now ready to plot all the data.
"""
for tagname_y in tagnames_y:

    ylabel = tagname_y
    if ylabel == "sphere2d_data_diff_prog_phi_pert.res_norm_linf":
        ylabel = r"$L_\infty$ error on geopotential $\Phi'$"
    elif ylabel == "sphere2d_data_diff_prog_vrt.res_norm_linf":
        ylabel = r"$L_\infty$ error on vorticity $\xi$"
    elif ylabel == "sphere2d_data_diff_prog_div.res_norm_linf":
        ylabel = r"$L_\infty$ error on divergence $\delta$"

    params = []
    params += [
            {
                "tagname_x": "runtime.timestep_size",
                "xlabel": "Timestep size $\Delta t$ (seconds)",
                "ylabel": ylabel,
                #"title": "Timestep size vs. error",
                "title": None,
                "xscale": "log",
                "yscale": "log",
            },
        ]

    params += [
            {
                "tagname_x": "output.simulation_benchmark_timings.main_timestepping",
                "xlabel": "Wallclock time in seconds",
                "ylabel": ylabel,
                #"title": "Wallclock time vs. error",
                "title": None,
                "xscale": "log",
                "yscale": "log",
            },
        ]


    for param in params:

        tagname_x = param["tagname_x"]
        xlabel = param["xlabel"]
        ylabel = param["ylabel"]
        title = param["title"]
        xscale = param["xscale"]
        yscale = param["yscale"]

        print("*"*80)
        print("Processing tag "+tagname_x)
        print("*"*80)



        """
        Plotting format
        """
        # Filter out errors beyond this value!
        def data_filter(x, y, job_data):
            if y == None:
                return True

            x = float(x)
            y = float(y)

            if math.isnan(y):
                return True

            if True:
                if "prog_phi_pert" in tagname_y:
                    if "l1" in tagname_y:
                        if y > 1e3:
                            print("Sorting out L1 data "+str(y))
                            return True
                    elif "l2" in tagname_y:
                        if y > 1e3:
                            print("Sorting out L2 data "+str(y))
                            return True
                    elif "linf" in tagname_y:
                        if y > 1e4:
                            print("Sorting out Linf data "+str(y))
                            return True
                    else:
                        raise Exception("Unknown y tag "+tagname_y)

                elif "prog_vrt" in tagname_y:
                    if "l1" in tagname_y:
                        if y > 1e2:
                            print("Sorting out L1 data "+str(y))
                            return True
                    elif "l2" in tagname_y:
                        if y > 1e2:
                            print("Sorting out L2 data "+str(y))
                            return True
                    elif "linf" in tagname_y:
                        if y > 1e3:
                            print("Sorting out Linf data "+str(y))
                            return True
                    else:
                        raise Exception("Unknown y tag "+tagname_y)

                elif "prog_div" in tagname_y:
                    if "l1" in tagname_y:
                        if y > 1e2:
                            print("Sorting out L1 data "+str(y))
                            return True
                    elif "l2" in tagname_y:
                        if y > 1e2:
                            print("Sorting out L2 data "+str(y))
                            return True
                    elif "linf" in tagname_y:
                        if y > 1e-5:
                            print("Sorting out Linf data "+str(y))
                            return True
                    else:
                        raise Exception("Unknown y tag "+tagname_y)
                else:
                    print("TODO")

            return False



        d = JobsData_GroupsPlottingScattered(
                job_groups,
                tagname_x,
                tagname_y,
                data_filter = data_filter
            )

        fileid = "output_plotting_"+tagname_x.replace(".", "-").replace("_", "-")+"_vs_"+tagname_y.replace(".", "-").replace("_", "-")+"_"+postprocessing_kind


        if True:
            #
            # Proper naming and sorting of each group
            # Also, sort out empty cases
            #

            # new data dictionary
            data_new = {}
            for key, data in d.data.items():

                if len(data["x_values"]) == 0:
                    continue

                use = False

                # Render ERK methods
                #if key.startswith("ERK") and "order=4" not in key:
                if key.startswith("ERK"):
                    use = True

                # Keep IRK-based methods
                if "IRK" in key:
                    use = True

                # Keep all EXP methods
                if "EXP" in key:
                    use = True

                # Keep REXI methods
                #if "REXI" in key:
                #    use = True

                if not use:
                    continue

                key_new = key[:]
                key_new = key_new.replace("order=", "o=")
                key_new = key_new.replace("__", "_")
                key_new = key_new.replace("legendre", "leg")
                key_new = key_new.replace("equidistant", "equid")

                # REXI cleanups
                key_new = key_new.replace("_BREXI_", " B-REXI ")
                key_new = key_new.replace("phi0_g_leg_", "N=")
                key_new = key_new.replace("_CIREXI_", " CI-REXI ")
                key_new = key_new.replace("phi0_128_10.0_30.0_h1", "N=128")

                key_new = key_new.replace("_nrm", "")
                key_new = key_new.replace("ADDT(lc,n)", "lc+n")
                key_new = key_new.replace("ADDT(lc,n)", "lc+n")

                key_new = key_new.replace(",o=2", "")

                key_new = key_new.replace("ln", "$L_g+L_c+N$")
                key_new = key_new.replace("lg", "$L_g$")
                key_new = key_new.replace("lc", "$L_c$")
                key_new = key_new.replace("n", "$N$")

                import re

                if re.match("^ERK", key_new):
                    data["plot_style"] = {"color": "darkgray"}
                    if "o=3" in key_new:
                        data["plot_style"].update({"marker": "^"})
                    elif "o=4" in key_new:
                        data["plot_style"].update({"marker": ">"})
                    else:
                        data["plot_style"].update({"marker": "v"})

                if re.match("SS.*EXP", key_new):
                    data["plot_style"] = {"color": "red"}
                    if re.match(".*ERK.*EXP", key_new):
                        data["plot_style"].update({"marker": "*"})
                    elif re.match(".*EXP.*ERK", key_new):
                        data["plot_style"].update({"marker": "o"})

                if re.match("SS.*IRK", key_new):
                    data["plot_style"] = {"color": "green"}
                    if re.match(".*ERK.*IRK", key_new):
                        data["plot_style"].update({"marker": "*"})
                    elif re.match(".*IRK.*ERK", key_new):
                        data["plot_style"].update({"marker": "o"})

                if re.match("ETDRK", key_new):
                    data["plot_style"] = {"color": "blue"}
                    data["plot_style"].update({"marker": "s"})


                # generate nice tex label
                data["label"] = key_new

                # copy data
                data_new[key_new] = copy.copy(data)


            # Copy back new data table
            d.data = data_new


            c = 0
            if True:
                # Sort elements
                data_new = {}

                # All SS IRK methods
                for key, data in d.data.items():
                    if re.match("SS.*IRK.*", data["label"]):
                        data_new[c] = data
                        c += 1

                # All ETDRK
                for key, data in d.data.items():
                    if re.match("ETDRK.*", data["label"]):
                        data_new[c] = data
                        c += 1

                # Everything which is left over
                for key, data in d.data.items():
                    found = False
                    for key, data2 in data_new.items():
                        if data["label"] == data2["label"]:
                            found = True
                            break

                    if not found:
                        data_new[c] = data
                        c += 1

                # Copy back new data table
                d.data = data_new

        p = Plotting_ScatteredData()


        def fun(p):
            from matplotlib import ticker
            from matplotlib.ticker import FormatStrFormatter

            plt.tick_params(axis="x", which="minor")
            p.ax.xaxis.set_minor_formatter(FormatStrFormatter("%.0f"))
            p.ax.xaxis.set_major_formatter(FormatStrFormatter("%.0f"))

            p.ax.xaxis.set_minor_locator(ticker.LogLocator(subs=[1.5, 2.0, 3.0, 5.0]))

            for tick in p.ax.xaxis.get_minor_ticks():
                tick.label1.set_fontsize(8) 

            plt.tick_params(axis="y", which="minor")
            p.ax.yaxis.set_minor_formatter(FormatStrFormatter("%.1e"))
            p.ax.yaxis.set_major_formatter(FormatStrFormatter("%.1e"))

            p.ax.yaxis.set_minor_locator(ticker.LogLocator(subs=[1.5, 2.0, 3.0, 5.0]))

            for tick in p.ax.yaxis.get_minor_ticks():
                tick.label1.set_fontsize(6) 



        annotate_text_template = "{:.1f} / {:.3f}"
        p.plot(
                data_plotting = d.get_data_float(),
                xlabel = xlabel,
                ylabel = ylabel,
                title = title,
                xscale = xscale,
                yscale = yscale,
                figsize = (5, 3),
                #annotate = True,
                #annotate_each_nth_value = 3,
                #annotate_fontsize = 6,
                #annotate_text_template = annotate_text_template,
                legend_fontsize = 7,
                grid = True,
                outfile = fileid+".pdf",
                #lambda_fun = fun,
            )

        print("Data plotting:")
        d.print()
        d.write(fileid+".csv")

    print("Info:")
    print("    NaN: Errors in simulations")
    print("    None: No data available")
