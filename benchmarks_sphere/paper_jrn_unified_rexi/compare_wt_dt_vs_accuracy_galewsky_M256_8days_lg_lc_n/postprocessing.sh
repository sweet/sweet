#! /usr/bin/env bash


mule.postprocessing.pickle.alljobs.sphere2d_data_norms_grid_space || exit 1

./postprocessing_plot_all.py
./postprocessing_plot_irk_exp.py
./postprocessing_plot_irk_rexi.py


