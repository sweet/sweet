#! /usr/bin/env python3

import sys
import numpy as np
import matplotlib.pyplot as plt

from mule.postprocessing.Sphere2DData import Sphere2DData
import mule.utils


input_filename = "job_bench_reference_REFJOB_RT_tsm_ERK_ln_o_4__dt00005.00_PAR_r00001_DIMS_space024/output_prog_vrt_t00000691200.00000000.sweet"
output_filename = "output_prog_vrt_t00000691200.00000000.pdf"


sphere2ddata = Sphere2DData(input_filename, setup_grid=True)
data_grid = sphere2ddata.data_grid

if sphere2ddata.file_info == None:
    raise Exception("TODO")
else:
    file_info = sphere2ddata.file_info


# Clear plot
plt.close()

fig, ax = plt.subplots(1, 1, figsize=(10, 4))

# Locations of ticks
xtickslocs = np.arange(data_grid.shape[1]) + 0.5
ytickslocs = np.arange(data_grid.shape[0]) + 0.5

# Labels of ticks
xticklabels = file_info['lons']
yticklabels = file_info['lats']

xticklabels = np.array([round(_, 1) for _ in xticklabels])
yticklabels = np.array([round(_, 1) for _ in yticklabels])

assert len(xtickslocs) == len(xticklabels)
assert len(ytickslocs) == len(yticklabels)

if True:
    """
    Cleanup ticks so that there are only Nx ticks
    """
    Nx = 16
    N = len(xticklabels)
    step = max(1, N//Nx)
    r = np.arange(Nx, dtype=int)*step
    xtickslocs = xtickslocs[r]
    xticklabels = xticklabels[r]

    Ny = 8
    N = len(yticklabels)
    step = max(1, N//Ny)
    r = np.arange(Ny, dtype=int)*step
    ytickslocs = ytickslocs[r]
    yticklabels = yticklabels[r]

# Make pixel centered around integer coordinates
extent = [-0.5, data_grid.shape[1]-0.5, data_grid.shape[0]-0.5, -0.5]
imhandler = ax.imshow(data_grid, cmap="viridis", extent=extent)


# Fontsize
fontsize = 12

# Colorbar
cbar = fig.colorbar(imhandler, ax=ax)
cbar.ax.tick_params(labelsize=fontsize) 


# Axis labels
#ax = fig.gca()

ax.set_xticks(xtickslocs)
ax.set_xticklabels([round(_, 1) for _ in xticklabels], fontsize=fontsize)
ax.set_xlabel("Longitude", fontsize=fontsize)

ax.set_yticks(ytickslocs)
ax.set_yticklabels([round(_, 1) for _ in yticklabels], fontsize=fontsize)
ax.set_ylabel("Latitude", fontsize=fontsize)


#input_filename_noext = mule.utils.remove_file_ending(input_filename)
#ax.set_title(input_filename_noext)


fig.tight_layout()

plt.savefig(output_filename)

